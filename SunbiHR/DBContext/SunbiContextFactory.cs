﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.DBContext
{
    public class SunbiContextFactory : IDesignTimeDbContextFactory<SunbiDBContext>
    {
        private readonly IConfiguration _Configuration;
        public SunbiContextFactory()
        {

        }
        public SunbiContextFactory(IConfiguration configuration)
        {
            _Configuration = configuration;
        }
        public SunbiDBContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SunbiDBContext>();
            optionsBuilder.UseMySQL(_Configuration.GetConnectionString("DevConnection"));

            return new SunbiDBContext(optionsBuilder.Options);
        }
    }
}
