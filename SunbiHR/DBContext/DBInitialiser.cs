﻿using Microsoft.AspNetCore.Identity;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.DBContext
{
    public static class DBInitialiser
    {
        public static async Task SeedEssentialsAsync(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            foreach (var item in Enum.GetNames(typeof(Roles)))
            {
                if (!roleManager.Roles.Any(x => x.Name ==item.ToString()))
                {
                    //Seed Roles
                    await roleManager.CreateAsync(new ApplicationRole() { Id=Guid.NewGuid().ToString(), Name= item.ToString() });

                }
            }
           
            if (await userManager.FindByNameAsync("superadmin@sunbi.com")==null)
            {
                var user = new ApplicationUser() { Id = Guid.NewGuid().ToString(), UserName = "superadmin@sunbi.com", Email = "superadmin@sunbi.com" };
                await userManager.CreateAsync(user,"qQ12!@");
                await userManager.AddToRoleAsync(user, Roles.SuperAdmin.ToString());
            }

        }

    }
}
