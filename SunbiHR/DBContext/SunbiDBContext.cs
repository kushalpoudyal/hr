﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Entity.UserDetails;

namespace SunbiHR.DBContext
{
    public class SunbiDBContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public SunbiDBContext()
        {

        }
        public SunbiDBContext(DbContextOptions<SunbiDBContext> options) : base(options)
        {

        }

        public DbSet<EmployeeInformation> EmployeeInformation { get; set; }
        public DbSet<EmployeePayment> EmployeePayment { get; set; }
        public DbSet<EmployeeTimline> EmployeeTimline { get; set; }
        public DbSet<ProjectInformation> ProjectInformation { get; set; }
        public DbSet<EmployeeProject> EmployeeProject { get; set; }
        public DbSet<TaskInformation> TaskInformation { get; set; }
        //public DbSet<TaskActivity> TaskActivity { get; set; }
        public DbSet<Milestone> Milestone { get; set; }
        public DbSet<ChangeLog> ChangeLog { get; set; }
        public DbSet<EmployeeUser> EmployeeUser { get; set; }
        public DbSet<EmployeeDailyReport> EmployeeDailyReport { get; set; }
        public DbSet<EmployeeLeaveRequest> EmployeeLeaveRequest { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<TaskEmployee> TaskEmployee { get; set; }
        public DbSet<DailyReportProject> DailyReportProject { get; set; }

        public async Task<int> CommitAsync()
        {
            var modifiedEntities = this.ChangeTracker.Entries()
                .Where(s => s.State == EntityState.Added || s.State == EntityState.Modified)
                .ToList();
            var now = DateTime.UtcNow;
            foreach (var modified in modifiedEntities)
            {
                if (modified.State == EntityState.Added)
                {
                    if (modified.Metadata.FindProperty("Id") == null)
                        continue;// to do fix 
                    //for newly added 
                    foreach (var prop in modified.OriginalValues.Properties)
                    {
                        if (modified.CurrentValues[prop] != null)
                        {
                            var originalValue = modified.OriginalValues[prop]?.ToString() ?? null;
                            var currentValue = modified.CurrentValues[prop].ToString() ?? null;
                            //var chnage = modified.Property("Id");

                            var Changelog = new ChangeLog()
                            {
                                PKId = modified.Property("Id")?.CurrentValue?.ToString() ?? Guid.NewGuid().ToString(),
                                EntityName = modified.Entity.GetType().Name,
                                PropertyName = prop.GetColumnName(),
                                OldValue = currentValue,
                                NewValue = currentValue,
                                DateChanged = now,
                                EntityState = modified.State
                            };
                            Add(Changelog);

                        }

                    }
                }
                else
                {

                    //for mdoified added 
                    foreach (var prop in modified.OriginalValues.Properties)
                    {
                        if (modified.CurrentValues[prop] != null)
                        {
                            var originalValue = modified.OriginalValues[prop]?.ToString() ?? null;
                            var currentValue = modified.CurrentValues[prop].ToString() ?? null;
                            if (originalValue == currentValue)
                            {
                                continue;
                            }


                            var Changelog = new ChangeLog()
                            {
                                PKId = modified.Property("Id").CurrentValue?.ToString() ?? Guid.NewGuid().ToString(),
                                EntityName = modified.Entity.GetType().Name,
                                PropertyName = prop.GetColumnName(),
                                OldValue = originalValue,
                                NewValue = currentValue,
                                DateChanged = now,
                                EntityState = modified.State
                            };
                            Add(Changelog);

                        }

                    }
                }
            }
            return await this.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            modelBuilder.Entity<ApplicationRole>().ToTable("ApplicationRole");

            modelBuilder.Entity<ApplicationUser>().Property(h => h.Id).HasMaxLength(36).IsRequired();
            modelBuilder.Entity<ApplicationRole>().Property(h => h.Id).HasMaxLength(36).IsRequired();
            modelBuilder.Entity<EmployeeProject>()
            .HasKey(bc => new { bc.EmployeeId, bc.ProjectId });
            modelBuilder.Entity<EmployeeProject>()
                .HasOne(bc => bc.EmployeeInformation)
                .WithMany(b => b.EmployeeProjects)
                .HasForeignKey(bc => bc.EmployeeId);
            modelBuilder.Entity<EmployeeProject>()
                .HasOne(bc => bc.ProjectInformation)
                .WithMany(c => c.EmployeeProjects)
                .HasForeignKey(bc => bc.ProjectId);
            modelBuilder.Entity<EmployeeUser>()
                .HasIndex(e => e.UserId)
                .IsUnique();
            

        }

    }
}
