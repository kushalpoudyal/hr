﻿using SunbiHR.Models.Entity.EmployeeInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.ViewModel
{
    public class AjaxResult<T> where T : class
    {
        public bool Status { get; set; }

        public string ErrorMsg { get; set; }

        public List<T> DataList { get; set; }
    }
}
