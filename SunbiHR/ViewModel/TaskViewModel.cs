﻿using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static SunbiHR.Models.Enum.TaskEnums;

namespace SunbiHR.ViewModel
{
    public class TaskViewModel
    {
        public string Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Display(Name = "Status")]

        public EnumTaskStatus TaskStatus { get; set; }
        [Display(Name = "Description")]
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required]
        public DateTime EndDate { get; set; }
        
        [Display(Name = "Due Date")]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? DueDate { get; set; }
        [Display(Name ="Project")]
        [Required]
        public string ProjectId { get; set; }
        [Display(Name = "Milestone")]
        public string MilestoneId { get; set; }
        [StringLength(1000)]
        public string Remarks { get; set; }

        public ProjectInformation ProjectInformation { get; set; }
        public Milestone Milestone { get; set; }

        public virtual ICollection<TaskEmployee> TaskEmployee { get; set; }

        [Display(Name = "Assigned Employee")]
        [Required]
        public string[] EmployeeIds { get; set; }
    }

}
