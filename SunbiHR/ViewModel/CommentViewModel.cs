﻿using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.ViewModel
{
    public class CommentViewModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public EnumRequestType EnumRequestType { get; set; }
        [Required]
        [StringLength(1000)]
        public string ApprovedRemarks { get; set; }

     
    }

}
