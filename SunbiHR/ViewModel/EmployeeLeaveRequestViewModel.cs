﻿using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.ViewModel
{
    public class EmployeeLeaveRequestViewModel
    {
        public String Id { get; set; }
        public DateTime CreatedOn { get; set; }
        [Required]
        [Display(Name = "Leave Type")]
        public LeaveType LeaveType { get; set; }

        [Display(Name = "Leave From")]
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime LeaveFrom { get; set; }

        [Display(Name = "Leave To")]
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime LeaveTo { get; set; }

        [Display(Name = "Reason")]
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }

        public bool IsApproved { get; set; }
        public bool IsRejected { get; set; }
        [Display(Name = "Employee Name")]
        [StringLength(36)]
        public string EmployeeId { get; set; }

        [Display(Name = "Approved By")]
        [StringLength(36)]
        public string EmployeeId_Approvedby { get; set; }

        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ApprovedDate { get; set; }

        [StringLength(2000)]
        public string ApprovedRemarks { get; set; }

        public int NoOfDays { get; set; }
        [Required]
        [Display(Name = "Leave Shift")]
        public LeavePeriod LeavePeriod { get; set; }

        public virtual EmployeeInformation EmployeeInformation { get; set; }
        public virtual EmployeeInformation EmployeeInformation1 { get; set; }

    }

}
