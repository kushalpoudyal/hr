﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.ViewModel
{

    public class RegistrationViewModel
    {
        [Required]
        public string Fullname { get; set; }
        public string Contact { get; set; }
        public string Country { get; set; }


        public string Organization { get; set; }


        public string WorkTitle { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]

        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Password do not match.")]
        public string ConfirmPassword { get; set; }

        public List<string> Roles { get; set; }
        public string BuyId { get; set; }
        public IFormFile Picture { get; set; }
        [Required]
        public string EmployeeId { get; set; }
        public bool IsActive { get; set; }
    }
}
