﻿using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.ProjectInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.ViewModel
{
    public class ReportViewModel
    {
        public ReportViewModel()
        {
            DailyReportProject = new HashSet<DailyReportProject>();
        }
        public string Id { get; set; }
        [Required]
        [StringLength(36)]
        public string Title { get; set; }
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }
        [Display(Name = "Employee Name")]
        [StringLength(36)]
        public string EmployeeId { get; set; }
        [Display(Name ="Date")]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime CreatedOn { get; set; }
        public virtual EmployeeInformation EmployeeInformation { get; set; }
        public virtual ICollection<DailyReportProject> DailyReportProject { get; set; }
        [Required]
        public string[] Projects { get; set; }
    }


}
