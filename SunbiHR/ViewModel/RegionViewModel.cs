﻿using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.ViewModel
{
    public class RegionViewModel
    {
        public string Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [StringLength(1000)]
        public string Remarks { get; set; }

    }

}
