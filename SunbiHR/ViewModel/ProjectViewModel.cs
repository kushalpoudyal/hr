﻿using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static SunbiHR.Models.Enum.Enums;

namespace SunbiHR.ViewModel
{
    public class ProjectViewModel
    {
        public ProjectViewModel()
        {
            this.EmployeeProjects = new HashSet<EmployeeProject>();
        }
        public string Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        [Required]
        public EnumProjectStatus Status { get; set; }

        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required]
        public DateTime StartedDate { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime EstimatedEndDate { get; set; }
        public string[] Employees { get; set; }
        public virtual ICollection<EmployeeProject> EmployeeProjects { get; set; }
        public virtual ICollection<Milestone> Milestone { get; set; }

    }
}
