﻿using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static SunbiHR.Models.Enum.Enums;

namespace SunbiHR.ViewModel
{
    public class MilestoneViewModel
    {
        public string Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? DueDate { get; set; }

        [StringLength(1000)]
        public string Deliverables { get; set; }

        [Required]
        public EnumMilestoneStatus Status { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required]
        public DateTime EstimatedStartDate { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required]
        public DateTime EstimatedEndDate { get; set; }

        [Required]
        public string ProjectId { get; set; }

        public ProjectInformation ProjectInformation { get; set; }

    }

}
