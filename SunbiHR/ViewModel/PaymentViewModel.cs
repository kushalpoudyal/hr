﻿using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.ViewModel
{
    public class PaymentViewModel
    {

        public string Id { get; set; }

        public string RefNo { get; set; }

        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }
        public MonthInEnglish PaymentMonth { get; set; }
        public int PaymentYear { get; set; }

        public string PaymentRemarks { get; set; }
        public decimal Amount { get; set; }

        public DateTime TransactionOn { get; set; }

        public string PaidBy { get; set; }
        public DateTime JoiningDate { get; set; }
        public string DueSince { get; set; }

        public List<EmployeePayment> PaymentHistory { get; set; }

        public decimal TotalDue { get; set; }
    }
}
