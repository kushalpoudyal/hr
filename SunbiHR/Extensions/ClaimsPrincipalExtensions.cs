﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
namespace SunbiHR.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            return GetClaimValueByClaimType(principal,ClaimTypes.NameIdentifier);
        }

        private static string GetClaimValueByClaimType(ClaimsPrincipal principal,string claimType)
        {
            return principal.FindFirst(claimType)?.Value;
        }

        public static string GetEmployeeId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "EmployeeId");
        }
        public static bool IsAdmin(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return principal.IsInRole("Administrator")|| principal.IsInRole("SuperAdmin");
        }
    }
}
