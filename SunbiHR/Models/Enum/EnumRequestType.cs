﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public enum EnumRequestType
    {
        Approved = 1,
        Rejected
    }
}
