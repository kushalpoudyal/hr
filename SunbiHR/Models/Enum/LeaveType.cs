﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public enum LeaveType
    {
        [Display(Name = "Sick Leave")]
        SickLeave = 1,
        [Display(Name = "Personal Leave")]
        PersonalLeave,
        [Display(Name = "Paid Leave")]
        PaidLeave,
        [Display(Name = "Unpaid Leave")]
        UnpaidLeave,
    }
}
