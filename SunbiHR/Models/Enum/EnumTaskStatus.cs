﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public class TaskEnums
    {
        public enum EnumTaskStatus
        {
            [Display(Name = "Open")]
            Open=1,
            [Display(Name = "Pending Start")]
            PendingStart = 2,
            [Display(Name = "Completed")]
            Completed=3,
            [Display(Name = "Cancelled")]
            Cancelled=4
        }

        public enum EnumTaskActivityActionType
        {     
            Pending = 1,
            Completed,
            Cancelled
        }

    }
}
