﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public enum Record_Status
    {
        Active = 1,
        InActive,
        UnAuthorized
    }
}
