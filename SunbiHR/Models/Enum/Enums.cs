﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public class Enums
    {
        public enum EnumMilestoneStatus
        {
            PendingStart=1,
            Active,
            Completed
        };
        public enum EnumProjectStatus
        {
            ResearchAndDevelopment = 1,
            InProduction,
            CompletedUnderReview,
            PendingApproval,
            Sales
        };
    }
}
