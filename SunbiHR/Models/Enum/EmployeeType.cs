﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public enum EmployeeType
    {
        [Display(Name = "Full-time")]
        FullTime = 1,
        [Display(Name = "Part-time")]
        PartTime,
        [Display(Name = "Daily")]
        Daily
    }
}
