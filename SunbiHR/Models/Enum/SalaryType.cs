﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public enum SalaryType
    {
        [Display(Name = "Weekly")]
        W = 1,
        [Display(Name = "Monthly")]
        M,
        [Display(Name = "Yearly")]
        Y
    }
}
