﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Enum
{
    public enum LeavePeriod
    {
        [Display(Name = "Full Time")]
        FullTime = 1,
        [Display(Name = "First Half")]
        FirstHalf,
        [Display(Name = "Second Half")]
        SecondHalf,

    }
}
