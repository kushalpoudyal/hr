﻿using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            //Id = Guid.NewGuid().ToString();
            CreatedOn = DateTime.Now;
            LastModifiedDate =DateTime.Now;
            ModifiedTimes = 1;
            Record_Status = Record_Status.Active;
            Deleted_Status = false;
        }

        [Required]
        [StringLength(36)]
        public string Id { get; set; }
        [Required]
        [StringLength(36)]
        public string CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int ModifiedTimes { get; set; }
        public Record_Status Record_Status { get; set; }
        [Column("Deleted_Status", TypeName = "bit")]
        public bool Deleted_Status { get; set; }
        [ForeignKey("CreatedById")]
        [AllowNull]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
