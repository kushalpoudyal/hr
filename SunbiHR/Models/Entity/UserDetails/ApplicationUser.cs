﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.UserDetails
{
    public class ApplicationUser : IdentityUser<string>
    {
        [StringLength(350)]
        public string Fullname { get; set; }
        [StringLength(150)]
        public string Contact { get; set; }
        [StringLength(150)]
        public string Country { get; set; }

        [StringLength(150)]
        public string Organization { get; set; }

        [StringLength(150)]
        public string WorkTitle { get; set; }
        public bool IsPasswordUpdated { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }


    }
}
