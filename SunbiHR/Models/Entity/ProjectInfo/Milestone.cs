﻿using SunbiHR.Models.Entity.TaskInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using static SunbiHR.Models.Enum.Enums;

namespace SunbiHR.Models.Entity.ProjectInfo
{
    public class Milestone : BaseEntity
    {
        public Milestone()
        {
            TaskInformation = new HashSet<TaskInformation>();
        }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Display(Name = "Due Date")]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? DueDate { get; set; }

        [StringLength(1000)]
        public string Deliverables { get; set; }

        [Required]
        public EnumMilestoneStatus Status { get; set; }

        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required]
        public DateTime EstimatedStartDate { get; set; }

        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required]
        public DateTime EstimatedEndDate { get; set; }
        [Display(Name = "Project")]
        [Required]
        [ForeignKey("ProjectInformation")]
        public string ProjectId { get; set; }

        public ProjectInformation ProjectInformation { get; set; }
        public ICollection<TaskInformation> TaskInformation { get; set; }

    }
}
