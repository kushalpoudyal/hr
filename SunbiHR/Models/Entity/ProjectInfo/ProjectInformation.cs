﻿using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.TaskInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static SunbiHR.Models.Enum.Enums;

namespace SunbiHR.Models.Entity.ProjectInfo
{
    public class ProjectInformation : BaseEntity
    {
        public ProjectInformation()
        {
            this.EmployeeProjects = new HashSet<EmployeeProject>();
            this.TaskInformation = new HashSet<TaskInformation>();
            this.Milestone = new HashSet<Milestone>();
            this.DailyReportProject = new HashSet<DailyReportProject>();
        }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime StartedDate { get; set; }

        public EnumProjectStatus Status { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime EstimatedEndDate { get; set; }
        public virtual ICollection<EmployeeProject> EmployeeProjects { get; set; }
        public virtual ICollection<TaskInformation> TaskInformation { get; set; }
        public virtual ICollection<Milestone> Milestone { get; set; }
        public virtual ICollection<DailyReportProject> DailyReportProject { get; set; }

        //public TimeSpan RemainingDaysTimeSpan => EstimatedEndDate.Date - DateTime.Now.Date;
        //public TimeSpan ElapsedDaysTimeSpan => StartedDate.Date - DateTime.Now.Date;
        //public TimeSpan TotalDaysSpecifiedTimeSpan => StartedDate.Date - EstimatedEndDate.Date;
        //public double RemainigDays => RemainingDaysTimeSpan.TotalDays;
        //public double ElapsedDays => ElapsedDaysTimeSpan.TotalDays;
        //public double TotalDaysSpecified => TotalDaysSpecifiedTimeSpan.TotalDays;
    }
}
