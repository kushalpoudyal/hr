﻿using SunbiHR.Models.Entity.ProjectInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity
{
    public class EmployeeProject
    {
        public string EmployeeId { get; set; }
        public string ProjectId { get; set; }
        public virtual EmployeeInformation EmployeeInformation { get; set; }
        public virtual ProjectInformation ProjectInformation { get; set; }
    }
}
