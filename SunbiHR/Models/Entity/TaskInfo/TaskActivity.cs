﻿using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.ProjectInfo
{
    public class TaskActivity : BaseEntity
    {
        [Required]
        [StringLength(200)]
        public string Subject { get; set; }
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        public TaskEnums.EnumTaskActivityActionType TaskRequestType { get; set; }
        public string TaskId { get; set; }
        public string EmployeeId { get; set; }

        [ForeignKey("TaskId")]
        public TaskInformation TaskInformation { get; set; }
        [ForeignKey("EmployeeId")]
        public EmployeeInformation EmployeeInformation { get; set; }

    }
}
