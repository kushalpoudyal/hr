﻿using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.ProjectInfo
{
    public class TaskEmployee : BaseEntity
    {
        public string TaskId { get; set; }
        public string EmployeeId { get; set; }

        [ForeignKey("TaskId")]
        public virtual TaskInformation TaskInformation { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual EmployeeInformation EmployeeInformation { get; set; }

    }
}
