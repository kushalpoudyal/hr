﻿using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.TaskInfo
{
    public class TaskInformation : BaseEntity
    {
        public TaskInformation()
        {
            this.TaskEmployee = new HashSet<TaskEmployee>();
        }
        [Display(Name = "Task Name")]
        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        [Display(Name = "Status")]

        public TaskEnums.EnumTaskStatus TaskStatus { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime StartDate { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Due Date")]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? DueDate { get; set; }

        [Display(Name = "Project")]
        [Required]
        [ForeignKey("ProjectInformation")]
        public string ProjectId { get; set; }
        [Display(Name = "Milestone")]
        [ForeignKey("Milestone")]
        public string MilestoneId { get; set; }
        [StringLength(1000)]
        public string Remarks { get; set; }

        public ProjectInformation ProjectInformation { get; set; }
        public Milestone Milestone { get; set; }

        public ICollection<TaskEmployee> TaskEmployee { get; set; }

    }
}
