﻿using SunbiHR.Models.Entity.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.EmployeeInfo
{
    public class EmployeeDailyReport : BaseEntity
    {
        public EmployeeDailyReport()
        {
            DailyReportProject = new HashSet<DailyReportProject>();
        }
        [Required]
        [StringLength(36)]
        public string Title { get; set; }
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }
        [Display(Name = "Employee Name")]
        [Required]
        [StringLength(36)]
        public string EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual EmployeeInformation EmployeeInformation { get; set; }
        public ICollection<DailyReportProject> DailyReportProject { get; set; }

    }
}
