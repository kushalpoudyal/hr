﻿using SunbiHR.Models.Entity.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.EmployeeInfo
{
    public class EmployeeTimline
    {
        [Required]
        [StringLength(36)]
        public string Id { get; set; }
        [StringLength(36)]

        public string EmployeeId { get; set; }
        [StringLength(200)]

        public string NewPosition { get; set; }

        public decimal Amount { get; set; }
        [StringLength(200)]
        public string Remarks { get; set; }

        public DateTime CommencedFrom { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool Current { get; set; }

        public string CreatedById { get; set; }

        public EmployeeTimline()
        {
            this.CreatedDate = DateTime.Now;
        }
        [ForeignKey("EmployeeId")]
        public virtual EmployeeInformation EmployeeInformation { get; set; }

        [ForeignKey("CreatedById")]
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
