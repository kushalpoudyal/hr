﻿using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.EmployeeInfo
{
    public class DailyReportProject : BaseEntity
    {
        [Required]
        [StringLength(36)]
        public string DailyReportId { get; set; }
        [Required]
        [StringLength(36)]
        public string ProjectId { get; set; }
        [ForeignKey("DailyReportId")]
        public virtual EmployeeDailyReport EmployeeDailyReport { get; set; }
        [ForeignKey("ProjectId")]
        public virtual ProjectInformation ProjectInformation { get; set; }
    }
}
