﻿using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity
{
    public class EmployeeInformation : BaseEntity
    {
        public EmployeeInformation()
        {
            this.EmployeeProjects = new HashSet<EmployeeProject>();
            this.EmployeeUser = new HashSet<EmployeeUser>();
            this.TaskEmployee = new HashSet<TaskEmployee>();
            this.EmployeeTimline = new HashSet<EmployeeTimline>();
        }
        [StringLength(200)]
        public string Fullname { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        [StringLength(20)]
        public string Contact { get; set; }
        [StringLength(20)]
        public string TemporaryContact { get; set; }
        [StringLength(2000)]
        public string PermanentFullAddress { get; set; }
        [StringLength(2000)]
        public string TemporaryFullAddress { get; set; }

        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(10)]
        public string BloodGroup { get; set; }
        [StringLength(100)]
        public string CitizenshipNo { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime CitizenshipIssuedDate { get; set; }
        [StringLength(100)]
        public string CitizenshipIssuedPlace { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime JoiningDate { get; set; }

        public EmployeeType EmpType { get; set; }
        [StringLength(100)]
        public string Position { get; set; }

        public decimal Salary { get; set; }
        public SalaryType SalaryType { get; set; }
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? PartedDate { get; set; }
        [Required]
        [StringLength(36)]
        public string RegionId { get; set; }

        //[Required]
        [StringLength(10)]
        public string PanNo { get; set; }

        [ForeignKey("RegionId")]
        public Region Region { get; set; }

        public virtual ICollection<EmployeeProject> EmployeeProjects { get; set; }
        public ICollection<EmployeeUser> EmployeeUser { get; set; }
        public ICollection<TaskEmployee> TaskEmployee { get; set; }
        public ICollection<EmployeeTimline> EmployeeTimline { get; set; }

        //
        [NotMapped]
        public string[] Projects { get; set; }

    }
}
