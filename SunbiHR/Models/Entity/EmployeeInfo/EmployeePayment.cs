﻿using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.EmployeeInfo
{
    public class EmployeePayment
    {
        [Required]
        [StringLength(36)]
        public string Id { get; set; }
        [Required]
        [StringLength(100)]
        public string RefNo { get; set; }
        [StringLength(36)]
        public string EmployeeId { get; set; }


        public MonthInEnglish PaymentMonth { get; set; }

        public int PaymentYear { get; set; }

        [StringLength(1000)]
        public string PaymentRemarks { get; set; }
        public decimal Amount { get; set; }

        public DateTime TransactionOn { get; set; }
        [StringLength(36)]
        public string PaidBy { get; set; }
        public EmployeePayment()
        {
            this.TransactionOn = DateTime.Now;
        }
        [ForeignKey("EmployeeId")]
        public virtual EmployeeInformation EmployeeInformation { get; set; }

        [ForeignKey("PaidBy")]
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
