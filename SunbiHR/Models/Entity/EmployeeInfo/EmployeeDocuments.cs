﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.EmployeeInfo
{
    public class EmployeeDocuments : BaseEntity
    {
        [Required]
        [StringLength(36)]
        public string EmployeeId { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(10)]
        public string FileExtension { get; set; }
        [StringLength(200)]
        public string FileURL { get; set; }
    }
}
