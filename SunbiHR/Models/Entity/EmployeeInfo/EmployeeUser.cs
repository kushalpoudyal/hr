﻿using SunbiHR.Models.Entity.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Models.Entity.EmployeeInfo
{
    public class EmployeeUser : BaseEntity
    {
        [Key]
        [StringLength(36)]
        public string UserId { get; set; }
        [Required]
        [StringLength(36)]
        public string EmployeeId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser1 { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual EmployeeInformation EmployeeInformation { get; set; }

    }
}
