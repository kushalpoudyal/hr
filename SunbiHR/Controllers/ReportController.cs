﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.Cms;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Helper;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using SunbiHR.ViewModel;

namespace SunbiHR.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ILogger<ReportController> _logger;

        public ReportController(
            SunbiDBContext context, 
            UserManager<ApplicationUser> userManager,
            ILogger<ReportController> logger)
        {
            _context = context;
            this.userManager = userManager;
            _logger = logger;
        }

        // GET: Project
        public async Task<IActionResult> Index(DateTime? fromDate, DateTime? toDate, ReportViewModel reportViewModel)
        {
            fromDate = fromDate.HasValue ? fromDate : DateTime.Now;
            toDate = toDate.HasValue ? toDate : DateTime.Now;
            InitCommon(reportViewModel);
            ViewData["fromDate"] = fromDate.Value.Date.ToString("yyyy-MM-dd");
            ViewData["toDate"] = toDate.Value.Date.ToString("yyyy-MM-dd");
            var employeeDailyReportList = _context.EmployeeDailyReport
                .Where(x => x.Deleted_Status == false)
                .Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeInformation).AsNoTracking().AsQueryable();
            employeeDailyReportList = employeeDailyReportList.Where(x => x.CreatedOn.Date >= fromDate.Value.Date && x.CreatedOn.Date <= toDate.Value.Date);

            if (!User.IsInRole("Administrator"))
            {
                employeeDailyReportList = employeeDailyReportList.Where(x => x.EmployeeId == User.GetEmployeeId());
            }
            if (!string.IsNullOrEmpty(reportViewModel?.EmployeeId))
            {
                employeeDailyReportList = employeeDailyReportList.Where(x => x.EmployeeId == reportViewModel.EmployeeId);

            }      
            return View(await employeeDailyReportList.ToListAsync());
        }

        // GET: Project/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeDailyReport = await _context.EmployeeDailyReport
                .Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeInformation)
                .Include(x => x.DailyReportProject)
                .ThenInclude(x=>x.ProjectInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeDailyReport == null)
            {
                return NotFound();
            }

            return View(employeeDailyReport);
        }

        public IActionResult Create()
        {
            var reportViewModel = new ReportViewModel();
            reportViewModel.Title = "Daily Report";
            reportViewModel.CreatedOn= DateTime.Now;
            InitCommon(reportViewModel);
            return View(reportViewModel);
        }

        private void InitCommon(ReportViewModel reportViewModel)
        {
            ViewData["EmployeeId"] = new SelectList(_context.EmployeeInformation, "Id", "Fullname", reportViewModel?.EmployeeId);
            //ViewData["Projects"] = new MultiSelectList(_context.ProjectInformation, "Id", "Title", reportViewModel?.DailyReportProject?.Select(x => x.ProjectId).ToArray());
            if (User.IsInRole("SuperAdmin"))
            {
                ViewData["Projects"] = new MultiSelectList(_context.ProjectInformation, "Id", "Title", reportViewModel?.DailyReportProject?.Select(x => x.ProjectId).ToArray());
            }
            else
            {
                ViewData["Projects"] = new MultiSelectList(_context.EmployeeProject.Where(x => x.EmployeeId == User.GetEmployeeId()).Include(x => x.ProjectInformation)
                                        .Select(x => x.ProjectInformation).ToList(), "Id", "Title", reportViewModel?.DailyReportProject?.Select(x => x.ProjectId).ToArray());
            } 

        }

        // POST: Project/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ReportViewModel reportViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var employeeDailyReport = new EmployeeDailyReport();
                    SetValuesFromViewModel(reportViewModel, employeeDailyReport);
                    employeeDailyReport.Id = Guid.NewGuid().ToString();
                    employeeDailyReport.CreatedById = User.GetUserId();
                    //employeeDailyReport.CreatedOn = DateTime.Now;
                    //employeeDailyReport.LastModifiedDate = DateTime.Now;
                    //employeeDailyReport.ModifiedTimes = 0;
                    //employeeDailyReport.Record_Status = Models.Enum.Record_Status.Active;
                    //employeeDailyReport.Deleted_Status = false;
                    employeeDailyReport.EmployeeId = User.GetEmployeeId();
                    
                    _context.Add(employeeDailyReport);
                    await _context.CommitAsync();
                    this.Notify("Daily Report Created Successfully", "Notice");
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception ex)
            {
                this.Notify("Error while creating daily report.", ExceptionHelper.GetMsg(ex), "error");
                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
            }

            InitCommon(reportViewModel);
            return View(reportViewModel);
        }

        private void SetValuesFromViewModel(ReportViewModel reportViewModel, EmployeeDailyReport employeeDailyReport)
        {
            employeeDailyReport.Title = reportViewModel.Title;
            employeeDailyReport.Description = reportViewModel.Description;
            var oldIds = employeeDailyReport.DailyReportProject.Where(x => !x.Deleted_Status)
                .Select(x => x.ProjectId).ToArray();//1,2,3
            var newlyAddedIds = reportViewModel.Projects.Except(oldIds);//4
            var removedIds = oldIds.Except(reportViewModel.Projects);//1
            //change status of removed project
            foreach (var item in removedIds)
            {
                var removedProject = employeeDailyReport.DailyReportProject.FirstOrDefault(x => x.ProjectId == item);
                if (removedProject != null)
                {
                    removedProject.Deleted_Status = true;
                    _context.DailyReportProject.Update(removedProject);
                }
            }
            //add new assigned project
            foreach (var item in newlyAddedIds)
            {
                employeeDailyReport.DailyReportProject.Add(new DailyReportProject()
                {
                    Id=Guid.NewGuid().ToString(),
                    ProjectId = item,
                    CreatedById = User.GetUserId(),
                });
            }
        }

        // GET: Project/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeDailyReport = await _context.EmployeeDailyReport
                .Include(x => x.EmployeeInformation)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (employeeDailyReport == null)
            {
                return NotFound();
            }
            var reportViewModel = new ReportViewModel()
            {
                Id = employeeDailyReport.Id,
                Description = employeeDailyReport.Description,
                Title = employeeDailyReport.Title,
                //EmployeeId=employeeDailyReport.EmployeeId
            };
            InitCommon(reportViewModel);
            return View(reportViewModel);
        }

        // POST: Project/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, ReportViewModel reportViewModel)
        {
            if (id != reportViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var OldData = await _context.EmployeeDailyReport.Include(x=>x.DailyReportProject).FirstOrDefaultAsync(x=>x.Id== id);
                    if (OldData == null)
                    {
                        return NotFound();
                    }
                    OldData.LastModifiedDate = DateTime.Now;
                    OldData.ModifiedTimes += 1;
                    SetValuesFromViewModel(reportViewModel, OldData);
                    _context.Update(OldData);
                    await _context.CommitAsync();
                    this.Notify("Daily Report Edited Successfully", "Notice");

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!EmployeeDailyReportExists(reportViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify("Error while updating region", ExceptionHelper.GetMsg(ex), "error");

                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }
                }
                catch (Exception ex)
                {
                    this.Notify("Error while updating region", ExceptionHelper.GetMsg(ex), "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            InitCommon(reportViewModel);
            return View(reportViewModel);
        }

        // GET: Project/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeDailyReport = await _context.EmployeeDailyReport
                .Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeDailyReport == null)
            {
                return NotFound();
            }

            return View(employeeDailyReport);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var employeeDailyReport = await _context.EmployeeDailyReport.FindAsync(id);
            if (employeeDailyReport == null)
            {
                return NotFound();
            }
            employeeDailyReport.Deleted_Status = true;
            //_context.EmployeeDailyReport.Remove(employeeDailyReport);
            _context.Update(employeeDailyReport);
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeDailyReportExists(string id)
        {
            return _context.EmployeeDailyReport.Any(e => e.Id == id);
        }
    }
}
