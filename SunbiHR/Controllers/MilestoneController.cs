﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.Cms;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Helper;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using SunbiHR.ViewModel;
using static SunbiHR.Models.Enum.Enums;

namespace SunbiHR.Controllers
{
    [Authorize(Roles = "Administrator,SuperAdmin")]
    public class MilestoneController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ILogger<MilestoneController> _logger;

        public MilestoneController(
            SunbiDBContext context,
            UserManager<ApplicationUser> userManager,
            ILogger<MilestoneController> logger)
        {
            _context = context;
            this.userManager = userManager;
            _logger = logger;
        }

        // GET: Project
        public async Task<IActionResult> Index()
        {

            var milestoneList = _context.Milestone
                .Where(x => x.Deleted_Status == false)
                .Include(e => e.ApplicationUser)
                .Include(x => x.ProjectInformation);
            return View(await milestoneList.ToListAsync());
        }

        // GET: Project/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var milestone = await _context.Milestone
                .Include(e => e.ApplicationUser)
                .Include(x => x.ProjectInformation)
                .Include(x => x.TaskInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (milestone == null)
            {
                return NotFound();
            }

            return View(milestone);
        }

        public IActionResult Create()
        {
            var milestoneViewModel = new MilestoneViewModel();
            InitCommon(milestoneViewModel);
            return View(milestoneViewModel);
        }

        private void InitCommon(MilestoneViewModel model)
        {
            ViewData["ProjectId"] = new SelectList(_context.ProjectInformation, "Id", "Title", model?.ProjectId);
            ViewData["Status"] = new SelectList(EnumHelper.GetItems<EnumMilestoneStatus>(), "Value", "Text", model?.Status);

        }

        // POST: Project/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MilestoneViewModel milestoneViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var milestone = new Milestone();
                    SetValuesFromViewModel(milestoneViewModel, milestone);
                    milestone.Id = Guid.NewGuid().ToString();
                    milestone.CreatedById = this.userManager.GetUserId(HttpContext.User);
                    milestone.CreatedOn = DateTime.Now;
                    milestone.LastModifiedDate = DateTime.Now;
                    milestone.ModifiedTimes = 0;
                    milestone.Record_Status = Models.Enum.Record_Status.Active;
                    milestone.Deleted_Status = false;

                    _context.Add(milestone);
                    await _context.CommitAsync();
                    if (Request.IsAjaxRequest())
                        return Json(new { status = true, msg = "Save Successfully" });
                    this.Notify("Milestone Created Successfully", "Notice");
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                this.Notify("Error while creating milestone" + ExceptionHelper.GetMsg(ex), "Error", "error");

                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
            }

            InitCommon(milestoneViewModel);
            return View(milestoneViewModel);
        }

        private static void SetValuesFromViewModel(MilestoneViewModel milestoneViewModel, Milestone milestone)
        {
            milestone.Id = milestoneViewModel.Id;
            milestone.Title = milestoneViewModel.Title;
            milestone.Deliverables = milestoneViewModel.Deliverables;
            milestone.EstimatedStartDate = milestoneViewModel.EstimatedStartDate;
            milestone.EstimatedEndDate = milestoneViewModel.EstimatedEndDate;
            milestone.DueDate = milestoneViewModel.DueDate;
            milestone.ProjectId = milestoneViewModel.ProjectId;
            milestone.Status = milestoneViewModel.Status;
        }

        // GET: Project/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var milestone = await _context.Milestone
                .Include(x => x.ProjectInformation)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (milestone == null)
            {
                return NotFound();
            }
            var milestoneViewModel = new MilestoneViewModel()
            {
                Id = milestone.Id,
                Deliverables = milestone.Deliverables,
                Title = milestone.Title,
                EstimatedStartDate = milestone.EstimatedStartDate,
                EstimatedEndDate = milestone.EstimatedEndDate,
                DueDate = milestone.DueDate,
                ProjectId = milestone.ProjectId,
                Status = milestone.Status,
            };
            InitCommon(milestoneViewModel);
            return View(milestoneViewModel);
        }

        // POST: Project/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, MilestoneViewModel milestoneViewModel)
        {
            if (id != milestoneViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var OldData = await _context.Milestone.FindAsync(id);
                    if (OldData == null)
                    {
                        return NotFound();
                    }
                    OldData.LastModifiedDate = DateTime.Now;
                    OldData.ModifiedTimes += 1;
                    SetValuesFromViewModel(milestoneViewModel, OldData);
                    _context.Update(OldData);
                    await _context.CommitAsync();
                    this.Notify("Milestone Updated Successfully", "Notice");

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!MilestoneExists(milestoneViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify("Error while creating milestone" + ExceptionHelper.GetMsg(ex), "Error", "error");

                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }
                }
                catch (Exception ex)
                {
                    this.Notify("Error while creating milestone" + ExceptionHelper.GetMsg(ex), "Error", "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            InitCommon(milestoneViewModel);
            return View(milestoneViewModel);
        }

        // GET: Project/Delete/5
        public async Task<JsonResult> GetMilestoneByProjectId(string id)
        {
            try
            {
                if (id == null)
                {
                    throw new Exception("Id is null");
                }

                var milestoneList = await _context.Milestone
                    .Include(e => e.ApplicationUser)
                    .Include(x => x.ProjectInformation)
                    .Where(m => m.Deleted_Status == false && m.ProjectId == id).
                    Select(x => new
                    {
                        text = x.Title,
                        id = x.Id
                    }).ToListAsync();
                return Json(new { Status = true, ErrorMsg = "", DataList = milestoneList });

            }
            catch (Exception ex)
            {

                return Json(new { Status = false, ErrorMsg = ExceptionHelper.GetMsg(ex), DataList = "" });
            }

        }

        public async Task<PartialViewResult> AjaxMilestoneList(string id)
        {
            try
            {
                if (id == null)
                {
                    throw new Exception("Id is null");
                }

                var milestoneList = await _context.Milestone
                    .Include(e => e.ApplicationUser)
                    .Include(x => x.ProjectInformation)
                    .Where(m => m.Deleted_Status == false && m.ProjectId == id)
                   .ToListAsync();
                return PartialView(milestoneList);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error while fetching milestone");
                //return Json(new { Status = false, ErrorMsg = ExceptionHelper.GetMsg(ex), DataList = "" });
            }
            return PartialView(new List<Milestone>());

        }

        // GET: Project/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var milestone = await _context.Milestone
                .Include(e => e.ApplicationUser)
                .Include(x => x.ProjectInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (milestone == null)
            {
                return NotFound();
            }

            return View(milestone);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var milestone = await _context.Milestone.FindAsync(id);
            if (milestone == null)
            {
                return NotFound();
            }
            milestone.Deleted_Status = true;
            //_context.Milestone.Remove(milestone);
            _context.Update(milestone);
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MilestoneExists(string id)
        {
            return _context.Milestone.Any(e => e.Id == id);
        }
    }
}
