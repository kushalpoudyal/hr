﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunbiHR.DAL;
using SunbiHR.DBContext;
using SunbiHR.Helper;
using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using SunbiHR.ViewModel;

namespace SunbiHR.Controllers
{
    [Authorize(Roles = "Administrator,SuperAdmin")]

    public class PaymentController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IPayService payService;
        private readonly ILogger<PaymentController> _logger;

        public PaymentController(
            SunbiDBContext context, 
            UserManager<ApplicationUser> userManager, 
            IPayService payService,
            ILogger<PaymentController> logger)
        {
            _context = context;
            this.userManager = userManager;
            this.payService = payService;
            _logger = logger;
        }

        // GET: Payment
        public async Task<IActionResult> Index()
        {
            var sunbiDBContext = _context.EmployeePayment.Include(e => e.ApplicationUser).Include(e => e.EmployeeInformation);
            return View(await sunbiDBContext.ToListAsync());
        }

        // GET: Payment/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeePayment = await _context.EmployeePayment
                .Include(e => e.ApplicationUser)
                .Include(e => e.EmployeeInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeePayment == null)
            {
                return NotFound();
            }

            return View(employeePayment);
        }

        // GET: Payment/Create
        public async Task<IActionResult> Create(string EmpId)
        {
            var model = new PaymentViewModel();
            model.EmployeeId = EmpId;
            model.Id = Guid.NewGuid().ToString();
            model.RefNo = UniqueCode.GetCode("SB");
            model.PaymentHistory = await _context.EmployeePayment.Where(x => x.EmployeeId == EmpId).Take(10).ToListAsync();
            model.EmployeeName = await _context.EmployeeInformation.Where(x => x.Id == EmpId).Select(x => x.Fullname).FirstOrDefaultAsync();
            model.JoiningDate = await _context.EmployeeInformation.Where(x => x.Id == EmpId).Select(x => x.JoiningDate).FirstOrDefaultAsync();
            model.PaymentYear = DateTime.Now.Year;
            model.TotalDue = await payService.TotalDueAmount(EmpId);
            model.PaidBy = this.userManager.GetUserId(User);
            model.TransactionOn = DateTime.Now;
            var duesInce = await payService.DueSince(EmpId);
            model.DueSince = duesInce.Month.ToString() + "/" + duesInce.Year.ToString();
            InitCommon(model);

            //ViewData["PaidBy"] = new SelectList(_context.Users, "Id", "Id");
            //ViewData["EmployeeId"] = new SelectList(_context.EmployeeInformation, "Id", "Id");
            return View(model);
        }

        private void InitCommon(PaymentViewModel model)
        {
            ViewData["PaymentMonth"] = new SelectList(EnumHelper.GetItems<MonthInEnglish>(), "Value", "Text", model?.PaymentMonth);
        }

        // POST: Payment/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,RefNo,EmployeeId,PaymentMonth,PaymentYear,PaymentRemarks,Amount,PaidBy,TransactionOn")] PaymentViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var employeePayment = new EmployeePayment();
                    employeePayment.Id = model.Id;
                    employeePayment.RefNo = model.RefNo;
                    employeePayment.EmployeeId = model.EmployeeId;
                    employeePayment.PaymentMonth = model.PaymentMonth;
                    employeePayment.PaymentYear = model.PaymentYear;
                    employeePayment.PaymentRemarks = model.PaymentRemarks;
                    employeePayment.Amount = model.Amount;
                    employeePayment.PaidBy = model.PaidBy;
                    employeePayment.TransactionOn = model.TransactionOn;

                    _context.Add(employeePayment);
                    await _context.CommitAsync();
                    this.Notify("Updated Successfully", "Notice");

                    return RedirectToAction("Create", "Payment", new { EmpId = model.EmployeeId });
                }
                catch (Exception ex)
                {
                    this.Notify( ExceptionHelper.GetMsg(ex),"Error", "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
               
            }
            InitCommon(model);
            //ViewData["PaidBy"] = new SelectList(_context.Users, "Id", "Id", employeePayment.PaidBy);
            //ViewData["EmployeeId"] = new SelectList(_context.EmployeeInformation, "Id", "Id", employeePayment.EmployeeId);
            return View(model);
        }


        // GET: Payment/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeePayment = await _context.EmployeePayment.FindAsync(id);
            if (employeePayment == null)
            {
                return NotFound();
            }
            ViewData["PaidBy"] = new SelectList(_context.Users, "Id", "Id", employeePayment.PaidBy);
            ViewData["EmployeeId"] = new SelectList(_context.EmployeeInformation, "Id", "Id", employeePayment.EmployeeId);
            return View(employeePayment);
        }

        // POST: Payment/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,RefNo,EmployeeId,PaymentMonth,PaymentRemarks,Amount,TransactionOn,PaidBy")] EmployeePayment employeePayment)
        {
            if (id != employeePayment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employeePayment);
                    await _context.CommitAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!EmployeePaymentExists(employeePayment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify(ExceptionHelper.GetMsg(ex), "Error", "error");

                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }
                }
                catch(Exception ex)
                {
                    this.Notify(ExceptionHelper.GetMsg(ex), "Error", "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PaidBy"] = new SelectList(_context.Users, "Id", "Id", employeePayment.PaidBy);
            ViewData["EmployeeId"] = new SelectList(_context.EmployeeInformation, "Id", "Id", employeePayment.EmployeeId);
            return View(employeePayment);
        }

        // GET: Payment/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeePayment = await _context.EmployeePayment
                .Include(e => e.ApplicationUser)
                .Include(e => e.EmployeeInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeePayment == null)
            {
                return NotFound();
            }

            return View(employeePayment);
        }

        // POST: Payment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var employeePayment = await _context.EmployeePayment.FindAsync(id);
            _context.EmployeePayment.Remove(employeePayment);
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeePaymentExists(string id)
        {
            return _context.EmployeePayment.Any(e => e.Id == id);
        }
    }
}
