﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.Cms;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Helper;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using SunbiHR.ViewModel;

namespace SunbiHR.Controllers
{
    [Authorize(Roles ="Administrator,SuperAdmin")]
    public class RegionController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ILogger<RegionController> _logger;

        public RegionController(
            SunbiDBContext context, 
            UserManager<ApplicationUser> userManager,
            ILogger<RegionController> logger)
        {
            _context = context;
            this.userManager = userManager;
            _logger = logger;
        }

        // GET: Region
        public async Task<IActionResult> Index(DateTime? fromDate, DateTime? toDate, RegionViewModel regionViewModel)
        {
            InitCommon(regionViewModel);

            var regionList = _context.Region
                .Where(x => x.Deleted_Status == false)
                .AsNoTracking().AsQueryable();
            return View(await regionList.ToListAsync());
        }

        // GET: Region/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var region = await _context.Region
                .FirstOrDefaultAsync(m => m.Id == id);
            if (region == null)
            {
                return NotFound();
            }

            return View(region);
        }

        public IActionResult Create()
        {
            var regionViewModel = new RegionViewModel();
            InitCommon(regionViewModel);
            return View(regionViewModel);
        }

        private void InitCommon(RegionViewModel regionViewModel)
        {

        }

        // POST: Region/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegionViewModel regionViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var region = new Region();
                    SetValuesFromViewModel(regionViewModel, region);
                    region.Id = Guid.NewGuid().ToString();
                    region.CreatedById = User.GetUserId();
                    region.CreatedOn = DateTime.Now;
                    region.LastModifiedDate = DateTime.Now;
                    region.ModifiedTimes = 0;
                    region.Record_Status = Models.Enum.Record_Status.Active;
                    region.Deleted_Status = false;
                    _context.Add(region);
                    await _context.CommitAsync();
                    this.Notify("Region Created Successfully", "Notice");
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception ex)
            {
                this.Notify("Error while creating region", ExceptionHelper.GetMsg(ex), "error");
                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
            }

            InitCommon(regionViewModel);
            return View(regionViewModel);
        }

        private static void SetValuesFromViewModel(RegionViewModel regionViewModel, Region region)
        {
            region.Title = regionViewModel.Title;
            region.Remarks = regionViewModel.Remarks;
        }

        // GET: Region/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var region = await _context.Region
                .FirstOrDefaultAsync(x => x.Id == id);
            if (region == null)
            {
                return NotFound();
            }
            var regionViewModel = new RegionViewModel()
            {
                Id = region.Id,
                Remarks = region.Remarks,
                Title = region.Title,
            };
            InitCommon(regionViewModel);
            return View(regionViewModel);
        }

        // POST: Region/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, RegionViewModel regionViewModel)
        {
            if (id != regionViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var OldData = await _context.Region.FindAsync(id);
                    if (OldData == null)
                    {
                        return NotFound();
                    }
                    OldData.LastModifiedDate = DateTime.Now;
                    OldData.ModifiedTimes += 1;
                    SetValuesFromViewModel(regionViewModel, OldData);
                    _context.Update(OldData);
                    await _context.CommitAsync();
                    this.Notify("Region Edited Successfully", "Notice");

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!RegionExists(regionViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify("Error while updating region", ExceptionHelper.GetMsg(ex), "error");

                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }
                }
                catch (Exception ex)
                {
                    this.Notify("Error while updating region", ExceptionHelper.GetMsg(ex), "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            InitCommon(regionViewModel);
            return View(regionViewModel);
        }

        // GET: Region/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var region = await _context.Region
                .FirstOrDefaultAsync(m => m.Id == id);
            if (region == null)
            {
                return NotFound();
            }

            return View(region);
        }

        // POST: Region/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var region = await _context.Region.FindAsync(id);
            if (region == null)
            {
                return NotFound();
            }
            region.Deleted_Status = true;
            //_context.Region.Remove(region);
            _context.Update(region);
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RegionExists(string id)
        {
            return _context.Region.Any(e => e.Id == id);
        }
    }
}
