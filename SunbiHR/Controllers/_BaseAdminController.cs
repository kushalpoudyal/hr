﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.Controllers
{
    [Authorize(Roles ="Administrator,SuperAdmin")]
    public abstract class _BaseAdminController : Controller
    {

    }
}
