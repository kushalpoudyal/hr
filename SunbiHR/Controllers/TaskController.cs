﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.Cms;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Helper;
using SunbiHR.Helper.Mail;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using SunbiHR.ViewModel;
using SunbiHR.ViewModel.Mail;
using static SunbiHR.Models.Enum.TaskEnums;

namespace SunbiHR.Controllers
{
    [Authorize(Roles ="Administrator,SuperAdmin")]
    public class TaskController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ILogger<TaskController> _logger;
        private readonly IMailService _mailService;

        public TaskController(
            SunbiDBContext context,
            UserManager<ApplicationUser> userManager,
            ILogger<TaskController> logger,
            IMailService mailService)
        {
            _context = context;
            this.userManager = userManager;
            _logger = logger;
            _mailService = mailService;
        }

        // GET: Task
        public async Task<IActionResult> Index()
        {

            var taskInformationList = _context.TaskInformation
                .Where(x => x.Deleted_Status == false)
                .Include(e => e.ApplicationUser)
                .Include(x => x.ProjectInformation)
                .Include(x => x.Milestone)
                .Where(x => x.CreatedById == userManager.GetUserId(User));
            return View(await taskInformationList.ToListAsync());
        }

        // GET: Task/Details/5
        [AllowAnonymous]
        [Authorize]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskInformation = await _context.TaskInformation
                .Include(e => e.ApplicationUser)
                .Include(x => x.ProjectInformation)
                .Include(x => x.Milestone)
                .Include(x => x.TaskEmployee)
                .ThenInclude(x => x.EmployeeInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (taskInformation == null)
            {
                return NotFound();
            }
            return View(taskInformation);
        }

        public IActionResult Create()
        {
            var taskViewModel = new TaskViewModel();
            InitCommon(taskViewModel);
            return View(taskViewModel);
        }

        private void InitCommon(TaskViewModel taskViewModel)
        {
            ViewData["ProjectId"] = new SelectList(_context.ProjectInformation, "Id", "Title", taskViewModel?.ProjectId);
            ViewData["EmployeeIds"] = new MultiSelectList(_context.EmployeeInformation, "Id", "Fullname", taskViewModel?.TaskEmployee?.Select(x => x.EmployeeId).ToArray());
            var milestoneListByProjectId = new List<Milestone>();
            if (taskViewModel?.ProjectId != null)
            {
                milestoneListByProjectId = _context.Milestone.Where(x => x.Deleted_Status == false && x.ProjectId == taskViewModel.ProjectId).ToList();
            }

            ViewBag.TaskStatus = new SelectList(Enum.GetValues(typeof(EnumTaskStatus)).Cast<EnumTaskStatus>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString(),
            }).ToList(), "Value", "Text", taskViewModel?.TaskStatus);
            ViewData["MilestoneId"] = new SelectList(milestoneListByProjectId, "Id", "Title", taskViewModel?.MilestoneId);
        }

        // POST: Task/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOrUpdate(TaskViewModel taskViewModel)
        {
            try
            {

                if (string.IsNullOrEmpty(taskViewModel.Id))
                {
                    var taskInformation = new TaskInformation();

                    taskInformation.CreatedById = User.GetUserId();
                    taskInformation.CreatedOn = DateTime.Now;
                    taskInformation.LastModifiedDate = DateTime.Now;
                    taskInformation.ModifiedTimes = 0;
                    taskInformation.Record_Status = Models.Enum.Record_Status.Active;
                    taskInformation.Deleted_Status = false;
                    SetValuesFromViewModel(taskViewModel, taskInformation);
                    taskInformation.Id = Guid.NewGuid().ToString();

                    _context.Add(taskInformation);

                    await _context.CommitAsync();
                    await SendMailToAssignedEmployee(taskViewModel);
                    return Json(new { status = true, msg = "Task Created Successfully" });
                }
                else
                {
                    var OldData = await _context.TaskInformation.Include(x => x.TaskEmployee)
                   .FirstOrDefaultAsync(x => x.Id == taskViewModel.Id);
                    if (OldData == null)
                    {
                        return Json(new { status = false, msg = "Data Not found." });
                    }
                    OldData.LastModifiedDate = DateTime.Now;
                    OldData.ModifiedTimes += 1;
                    SetValuesFromViewModel(taskViewModel, OldData);
                    _context.Update(OldData);

                    await _context.CommitAsync();
                    await SendMailToAssignedEmployee(taskViewModel);

                    return Json(new { status = true, msg = "Task Updated Successfully" });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                return Json(new { status = false, msg = ExceptionHelper.GetMsg(ex) });

            }

        }

        private async Task SendMailToAssignedEmployee(TaskViewModel taskViewModel)
        {
            var AssignedEmployeeEmail = _context.EmployeeInformation.Where(x => taskViewModel.EmployeeIds.Contains(x.Id)).Select(x => x.Email).ToArray();
            var duedateHaveValue = taskViewModel.DueDate.HasValue ? (" with due date" + taskViewModel.DueDate.Value.ToString("yyyy-MM-dd")) : "";
            var msg = "";
            foreach (var item in AssignedEmployeeEmail)
            {
                try
                {
                    var mailRequest = new MailRequest
                    {
                        Subject = "New Task has been Assigned",
                        Body = $"<h3>Dear User,</h3><p>New Task { taskViewModel.Title} has been assigned to you{ duedateHaveValue} .</p>",
                        ToEmail = item
                    };
                    await _mailService.SendEmailAsync(mailRequest);
                }
                catch (Exception ex)
                {
                    msg = "Error while sending mail" + ExceptionHelper.GetMsg(ex);
                    _logger.LogError(ex, "Error while sending task assigned mail.");
                }
            }
        }

        public async Task<ActionResult> AjaxTaskList(string id)
        {
            try
            {
                if (id == null)
                {
                    throw new Exception("Id is null");
                }

                var taskList = await _context.TaskInformation
                    .Include(e => e.ApplicationUser)
                    .Include(x => x.ProjectInformation)
                    .Where(m => m.Deleted_Status == false && m.ProjectId == id).
                   ToListAsync();
                return PartialView(taskList);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error fetching taskdata");
            }
            return PartialView(new List<TaskInformation>());

        }
        public async Task<ActionResult> AjaxActiveTaskList(string id)
        {
            try
            {
                if (id == null)
                {
                    throw new Exception("Id is null");
                }

                var taskList = await _context.TaskInformation
                    .Include(e => e.ApplicationUser)
                    .Include(x => x.ProjectInformation)
                    .Where(m => m.Deleted_Status == false && m.ProjectId == id && m.TaskStatus==EnumTaskStatus.Open)
                    .ToListAsync();
                return PartialView(taskList);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error fetching taskdata");
            }
            return PartialView(new List<TaskInformation>());

        }

        // POST: Task/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TaskViewModel taskViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var taskInformation = new TaskInformation();
                    SetValuesFromViewModel(taskViewModel, taskInformation);
                    taskInformation.Id = Guid.NewGuid().ToString();
                    taskInformation.CreatedById = User.GetUserId();
                    taskInformation.CreatedOn = DateTime.Now;
                    taskInformation.LastModifiedDate = DateTime.Now;
                    taskInformation.ModifiedTimes = 0;
                    taskInformation.Record_Status = Models.Enum.Record_Status.Active;
                    taskInformation.Deleted_Status = false;

                    _context.Add(taskInformation);
                    await _context.CommitAsync();
                    var AssignedEmployeeEmail = _context.EmployeeInformation.Where(x => taskViewModel.EmployeeIds.Contains(x.Id)).Select(x => x.Email).ToArray();
                    var duedateHaveValue = taskViewModel.DueDate.HasValue ? (" with due date" + taskViewModel.DueDate.Value.ToString("yyyy-MM-dd")) : "";
                    var msg = "";
                    foreach (var item in AssignedEmployeeEmail)
                    {
                        try
                        {
                            var mailRequest = new MailRequest
                            {
                                Subject = "New Task has been Assigned",
                                Body = $"<h3>Dear User,</h3><p>New Task { taskViewModel.Title} has been assigned to you{ duedateHaveValue} .</p>",
                                ToEmail = item
                            };
                            await _mailService.SendEmailAsync(mailRequest);
                        }
                        catch (Exception ex)
                        {
                            msg = "Error while sending mail" + ExceptionHelper.GetMsg(ex);
                            _logger.LogError(ex, "Error while sending task assigned mail.");
                        }
                    }

                    this.Notify("Task Created Succesfully." + msg, "Notice", "success");

                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                this.Notify("Error while creating Task", "Error", "error");

                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
            }

            InitCommon(taskViewModel);
            return View(taskViewModel);
        }

        private void SetValuesFromViewModel(TaskViewModel taskViewModel, TaskInformation taskInformation)
        {
            taskInformation.Id = taskViewModel.Id;
            taskInformation.Title = taskViewModel.Title;
            taskInformation.Description = taskViewModel.Description;
            taskInformation.StartDate = taskViewModel.StartDate;
            taskInformation.EndDate = taskViewModel.EndDate;
            taskInformation.MilestoneId = taskViewModel.MilestoneId;
            taskInformation.Remarks = taskViewModel.Remarks;
            taskInformation.ProjectId = taskViewModel.ProjectId;
            taskInformation.TaskStatus = taskViewModel.TaskStatus;
            taskInformation.DueDate = taskViewModel.DueDate;
            //_context.TaskEmployee.RemoveRange(taskInformation.TaskEmployee);
            //new 2,3,4
            var oldIds = taskInformation.TaskEmployee.Where(x => x.Deleted_Status)
                .Select(x => x.EmployeeId).ToArray();//1,2,3
            var newlyAddedIds = taskViewModel.EmployeeIds.Except(oldIds);//4
            var removedIds = oldIds.Except(taskViewModel.EmployeeIds);//1
            //change status of removed employees
            foreach (var item in removedIds)
            {
                var removedEmployee = taskInformation.TaskEmployee.FirstOrDefault(x => x.EmployeeId == item);
                if (removedEmployee != null)
                {
                    removedEmployee.Deleted_Status = true;
                    _context.TaskEmployee.Update(removedEmployee);
                }
            }
            //add new assigned employees
            foreach (var item in newlyAddedIds)
            {
                taskInformation.TaskEmployee.Add(new TaskEmployee()
                {
                    EmployeeId = item,
                    Id = Guid.NewGuid().ToString(),
                    CreatedById = User.GetUserId(),
                    CreatedOn = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    ModifiedTimes = 0,
                    Record_Status = Models.Enum.Record_Status.Active,
                    Deleted_Status = false,
                });
            }
        }


        // GET: Task/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskInformation = await _context.TaskInformation
                .Include(x => x.ProjectInformation)
                .Include(x => x.TaskEmployee)
                .Include(x => x.Milestone)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (taskInformation == null)
            {
                return NotFound();
            }
            var taskViewModel = new TaskViewModel()
            {
                Id = taskInformation.Id,
                Description = taskInformation.Description,
                Title = taskInformation.Title,
                StartDate = taskInformation.StartDate,
                EndDate = taskInformation.EndDate,
                MilestoneId = taskInformation.MilestoneId,
                Remarks = taskInformation.Remarks,
                ProjectId = taskInformation.ProjectId,
                DueDate = taskInformation.DueDate,
                TaskStatus = taskInformation.TaskStatus,
            };
            InitCommon(taskViewModel);
            return View(taskViewModel);
        }

        // POST: Task/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, TaskViewModel taskViewModel)
        {
            if (id != taskViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var OldData = await _context.TaskInformation.Include(x => x.TaskEmployee)
                        .FirstOrDefaultAsync(x => x.Id == id);
                    if (OldData == null)
                    {
                        return NotFound();
                    }
                    OldData.LastModifiedDate = DateTime.Now;
                    OldData.ModifiedTimes += 1;
                    SetValuesFromViewModel(taskViewModel, OldData);
                    _context.Update(OldData);
                    await _context.CommitAsync();
                    this.Notify("Task Edited Successfully", "Notice");
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!TaskInformationExists(taskViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify("Error while updating task " + ExceptionHelper.GetMsg(ex), "Error", "error");

                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }
                }
                catch (Exception ex)
                {
                    this.Notify("Error while updating task " + ExceptionHelper.GetMsg(ex), "Error", "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            InitCommon(taskViewModel);
            return View(taskViewModel);
        }
        [AllowAnonymous]
        [Authorize]

        public async Task<IActionResult> MarkComplete(string id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var OldData = await _context.TaskInformation.Include(x => x.TaskEmployee).FirstOrDefaultAsync(x => x.Id == id);
                if (OldData == null)
                {
                    return NotFound();
                }
                //valdiate if it current user is assigned user or admin
                if (!OldData.TaskEmployee.Any(x => x.EmployeeId == User.GetEmployeeId()) && !User.IsAdmin())
                {
                    throw new Exception("Only admin or assigned user can mark complete the task.");
                }

                OldData.TaskStatus = EnumTaskStatus.Completed;
                OldData.LastModifiedDate = DateTime.Now;
                OldData.ModifiedTimes += 1;
                _context.Update(OldData);
                await _context.CommitAsync();
                this.Notify("Task updated Successfully", "Notice");

            }
            catch (Exception ex)
            {
                this.Notify(ExceptionHelper.GetMsg(ex), "Error", "error");

                _logger.LogError(ex, "Error on mark complete");
            }
            if (!User.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction(nameof(Index));

        }

        // GET: Task/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskInformation = await _context.TaskInformation
                .Include(e => e.ApplicationUser)
                .Include(x => x.ProjectInformation)
                .Include(x => x.Milestone)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (taskInformation == null)
            {
                return NotFound();
            }

            return View(taskInformation);
        }

        // POST: Task/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var taskInformation = await _context.TaskInformation.FindAsync(id);
            if (taskInformation == null)
            {
                return NotFound();
            }
            taskInformation.Deleted_Status = true;
            //_context.TaskInformation.Remove(taskInformation);
            _context.Update(taskInformation);
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TaskInformationExists(string id)
        {
            return _context.TaskInformation.Any(e => e.Id == id);
        }
    }
}
