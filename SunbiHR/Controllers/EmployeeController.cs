﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.Cms;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Helper;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;

namespace SunbiHR.Controllers
{
    [Authorize(Roles = "Administrator,SuperAdmin")]
    public class EmployeeController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ILogger<EmployeeController> _logger;

        public EmployeeController(
            SunbiDBContext context,
            UserManager<ApplicationUser> userManager,
            ILogger<EmployeeController> logger)
        {
            _context = context;
            this.userManager = userManager;
            _logger = logger;
        }

        // GET: Employee
        public async Task<IActionResult> Index()
        {
            var sunbiDBContext = _context.EmployeeInformation.Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeProjects).ThenInclude(x => x.ProjectInformation);
            return View(await sunbiDBContext.ToListAsync());
        }

        // GET: Employee/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeInformation = await _context.EmployeeInformation
                .Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeProjects)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeInformation == null)
            {
                return NotFound();
            }
            ViewBag.EmployeeType = Enum.GetValues(typeof(EmployeeType)).Cast<EmployeeType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.Gender = Enum.GetValues(typeof(Gender)).Cast<Gender>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.SalaryType = Enum.GetValues(typeof(SalaryType)).Cast<SalaryType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();

            return View(employeeInformation);
        }

        [AllowAnonymous]
        public IActionResult Create()
        {
            var model = new EmployeeInformation();
            InitCommon(model);

            return View(model);
        }

        private void InitCommon(EmployeeInformation model)
        {
            model.Id = Guid.NewGuid().ToString();
            model.DateOfBirth = DateTime.Now;
            model.JoiningDate = DateTime.Now;
            model.CreatedById = this.userManager.GetUserId(HttpContext.User);
            model.CreatedOn = DateTime.Now;
            model.LastModifiedDate = DateTime.Now;
            model.ModifiedTimes = 0;
            model.Record_Status = Models.Enum.Record_Status.Active;
            model.Deleted_Status = false;
            ViewBag.EmployeeType = Enum.GetValues(typeof(EmployeeType)).Cast<EmployeeType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.Gender = Enum.GetValues(typeof(Gender)).Cast<Gender>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.SalaryType = Enum.GetValues(typeof(SalaryType)).Cast<SalaryType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewData["Projects"] = new MultiSelectList(_context.ProjectInformation, "Id", "Title", model?.EmployeeProjects.Select(x => x.ProjectId).ToArray());
            ViewData["RegionId"] = new SelectList(_context.Region, "Id", "Title", model?.RegionId);
        }

        // POST: Employee/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmployeeInformation employeeInformation)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (employeeInformation.Projects != null)
                    {
                        foreach (var projectID in employeeInformation.Projects)
                        {
                            employeeInformation.EmployeeProjects.Add(new EmployeeProject()
                            {
                                ProjectId = projectID
                            });
                        }
                    }
                    var Timeline = new EmployeeTimline();
                    Timeline.Id = Guid.NewGuid().ToString();
                    //Timeline.EmployeeId = employeeInformation.Id;
                    Timeline.NewPosition = employeeInformation.Position;
                    Timeline.Remarks = "Initial";
                    Timeline.Current = true;
                    Timeline.Amount = employeeInformation.Salary;
                    Timeline.CreatedById = this.userManager.GetUserId(HttpContext.User);
                    Timeline.CommencedFrom = employeeInformation.JoiningDate;

                    employeeInformation.EmployeeTimline.Add(Timeline);
                    _context.EmployeeInformation.Add(employeeInformation);
                    await _context.CommitAsync();

                    this.Notify("Employee Created Successfully", "Notice");

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    this.Notify("Error while creating employee.", ExceptionHelper.GetMsg(ex), "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }

            }
            InitCommon(employeeInformation);
            return View(employeeInformation);
        }

        public async Task<IActionResult> Review(string EmpId)
        {
            var model = new EmployeeTimline();
            model.EmployeeId = EmpId;
            model.Id = Guid.NewGuid().ToString();
            model.CommencedFrom = DateTime.Now;
            ViewBag.EmployeeName = await _context.EmployeeInformation.Where(x => x.Id == EmpId).Select(x => x.Fullname).FirstOrDefaultAsync();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Review(EmployeeTimline employeeTimline)
        {
            try
            {
                var employee = await _context.EmployeeInformation.Include(x => x.EmployeeTimline).FirstOrDefaultAsync(x => x.Id == employeeTimline.EmployeeId);
                if (employee == null)
                {
                    throw new Exception("Employee not found");
                }
                //change current to false of old
                foreach (var item in employee.EmployeeTimline.Where(x => x.Current == true))
                {
                    item.Current = false;
                    _context.Entry(item).State = EntityState.Modified;
                }
                employee.Salary = employeeTimline.Amount;
                var Timeline = new EmployeeTimline();
                Timeline.Id = Guid.NewGuid().ToString();
                Timeline.EmployeeId = employeeTimline.EmployeeId;
                Timeline.NewPosition = employeeTimline.NewPosition;
                Timeline.Remarks = employeeTimline.Remarks;
                Timeline.Current = true;
                Timeline.Amount = employeeTimline.Amount;
                Timeline.CreatedById = User.GetUserId();
                Timeline.CommencedFrom = employeeTimline.CommencedFrom;
                employee.EmployeeTimline.Add(Timeline);

                _context.Entry(employee).State = EntityState.Modified;
                await _context.CommitAsync();
                this.Notify("Employee Reviewed Successfully", "Notice");

            }
            catch (Exception ex)
            {
                this.Notify("Error while reviewing employee" + ExceptionHelper.GetMsg(ex), "Error", "error");

                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
            }
            return RedirectToAction(nameof(Index));
        }
        // GET: Employee/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeInformation = await _context.EmployeeInformation.FindAsync(id);
            if (employeeInformation == null)
            {
                return NotFound();
            }
            ViewData["CreatedById"] = new SelectList(_context.Users, "Id", "Id", employeeInformation.CreatedById);
            return View(employeeInformation);
        }

        // POST: Employee/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Fullname,DateOfBirth,Gender,Contact,TemporaryContact,PermanentFullAddress,TemporaryFullAddress,Email,JoiningDate,EmpType,Position,Salary,SalaryType,PartedDate,Id,CreatedById,CreatedOn,LastModifiedDate,ModifiedTimes,Record_Status,Deleted_Status")] EmployeeInformation employeeInformation)
        {
            if (id != employeeInformation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employeeInformation);
                    await _context.CommitAsync();
                    this.Notify("Employee Edited Successfully", "Notice");

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!EmployeeInformationExists(employeeInformation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify("Error while updating employee" + ExceptionHelper.GetMsg(ex), "Error", "error");

                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }
                }
                catch (Exception ex)
                {
                    this.Notify("Error while updating employee" + ExceptionHelper.GetMsg(ex), "Error", "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedById"] = new SelectList(_context.Users, "Id", "Id", employeeInformation.CreatedById);
            return View(employeeInformation);
        }

        // GET: Employee/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeInformation = await _context.EmployeeInformation
                .Include(e => e.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeInformation == null)
            {
                return NotFound();
            }

            return View(employeeInformation);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var employeeInformation = await _context.EmployeeInformation.FindAsync(id);
            employeeInformation.Deleted_Status = true;
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeInformationExists(string id)
        {
            return _context.EmployeeInformation.Any(e => e.Id == id);
        }
    }
}
