﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunbiHR.DAL;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Helper;
using SunbiHR.Helper.Mail;
using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.ViewModel;
using SunbiHR.ViewModel.Mail;

namespace SunbiHR.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly SunbiDBContext _context;
        private readonly IMailService _mailService;
        private readonly ILogger<AccountController> _logger;
        public AccountController(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            SignInManager<ApplicationUser> signInManager,
            SunbiDBContext context,
            IMailService mailService,
            ILogger<AccountController> _logger)
        {
            this.userManager = userManager;
            _roleManager = roleManager;
            this.signInManager = signInManager;
            this._context = context;
            _mailService = mailService;
            this._logger = _logger;
        }
        [Authorize(Roles = "Administrator,SuperAdmin")]
        //[ContentAuthorize(ControllerName = "Account", UserType = Models.Enum.UserType.Admin)]
        public IActionResult Register(string BuyId)
        {
            var model = new RegistrationViewModel();
            InitCommon(model);
            model.BuyId = BuyId;
            return View(model);
        }

        private void InitCommon(RegistrationViewModel model)
        {
            ViewData["EmployeeId"] = new SelectList(_context.EmployeeInformation.Include(x => x.EmployeeUser).Where(x => !x.Deleted_Status && !x.EmployeeUser.Any()), "Id", "Fullname", model?.EmployeeId);
            ViewData["Roles"] = new MultiSelectList(_roleManager.Roles.ToList(), "Name", "Name", model?.Roles?.ToArray());
        }
        [Authorize(Roles = "Administrator,SuperAdmin")]
        public async Task<IActionResult> Index()
        {
            var userList = await userManager.Users.ToListAsync();
            return View(userList);
        }

        [Authorize(Roles = "Administrator,SuperAdmin")]
        [HttpPost]
        public async Task<IActionResult> Register(RegistrationViewModel model)
        {
            var UserType = string.Empty;
            model.Password = RandomPasswordHelper.GeneratePassword();
            model.ConfirmPassword = model.Password;
            ModelState.Clear();
            TryValidateModel(model);
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { Id = Guid.NewGuid().ToString(), UserName = model.Email, Email = model.Email };
                var result = await userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    user.Fullname = model.Fullname;
                    user.Contact = model.Contact;
                    user.Country = model.Country;
                    user.Organization = model.Organization;
                    user.WorkTitle = model.WorkTitle;
                    user.IsActive = model.IsActive;

                    //user.IsPasswordUpdated = User.Identity.IsAuthenticated == true ? true : false;

                    _context.Users.Update(user);
                    await _context.CommitAsync();
                    //add roles                
                    await userManager.AddToRolesAsync(user, model.Roles);
                    //add employee user relation
                    var employeeUser = new EmployeeUser()
                    {
                        UserId = user.Id,
                        EmployeeId = model.EmployeeId,
                        CreatedOn = DateTime.Now,
                        LastModifiedDate = DateTime.Now,
                        ModifiedTimes = 0,
                        Deleted_Status = false,
                        CreatedById = User.GetUserId(),
                        Record_Status = Models.Enum.Record_Status.Active,
                        Id = Guid.NewGuid().ToString()
                    };
                    _context.Add(employeeUser);
                    await _context.CommitAsync();

                    try
                    {
                        MailRequest mailRequest = new MailRequest()
                        {
                            ToEmail = model.Email,
                            Body = "<h1>Welcome to SunBI</h1><p>You have been registered.</p><p>Email:" + model.Email + "</p><p>Password:" + model.Password + "</p>",
                            Subject = "Account Created",
                        };

                        await this._mailService.SendEmailAsync(mailRequest);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Error while sending mail.");
                    }

                    return RedirectToAction("Index", "Home");

                }
                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("", item.Description);
                }
            }
            InitCommon(model);
            return View(model);
        }

        [Authorize(Roles = "Administrator,SuperAdmin")]

        public async Task<IActionResult> EditUser(string Id)
        {
            var model = new RegistrationViewModel();

            var usr = await _context.Users.FindAsync(Id);

            model.Fullname = usr.Fullname;
            model.Contact = usr.Contact;
            model.Country = usr.Country;
            model.Email = usr.Email;
            return View(model);
        }
        public async Task<IActionResult> Details(string Id)
        {
            var model = new RegistrationViewModel();

            var usr = await _context.Users.FindAsync(Id);

            model.Fullname = usr.Fullname;
            model.Contact = usr.Contact;
            model.Country = usr.Country;
            model.Email = usr.Email;
            model.IsActive = usr.IsActive;
            model.Organization = usr.Organization;
            model.WorkTitle = usr.WorkTitle;




            return View(model);
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

                if (result.Succeeded)
                {
                    var p = model.Password;
                    var usrdetails = await _context.Users.Where(x => x.UserName == model.Email).FirstOrDefaultAsync();
                    if (!usrdetails.IsActive)
                    {
                        throw new Exception("Please contact Adminstrator.");
                    }
                    if (usrdetails.IsDeleted)
                    {
                        throw new Exception("No User found with current email.");
                    }
                    var usrRole = await this.userManager.GetRolesAsync(usrdetails);
                    if (!usrRole.Any())
                    {
                        throw new Exception("No roles assigned.Please contact Administrator.");
                    }
                    // this.emailService.Mail_Alert(model.Email, "Login Detected", "<h4>You recently have been found to be logged in the Boxklip Application.</h4>");

                    if (usrdetails.IsPasswordUpdated)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("ChangePassword", "Account");
                    }
                }

                ModelState.AddModelError("", "Invalid Login Attempt");

            }
            return View(model);
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Invalid Credentials, Please try again!");

            }
            var UserT = await _context.Users.Where(x => x.Id == (this.userManager.GetUserId(User))).FirstOrDefaultAsync();
            var result = await this.userManager.ChangePasswordAsync(UserT, model.OldPassword, model.NewPassword);

            if (result.Succeeded)
            {
                UserT.IsPasswordUpdated = true;
                await userManager.UpdateAsync(UserT);
                return RedirectToAction("Index", "Home");
            }
            AddErrors(result);
            return View(model);

        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await userManager.FindByEmailAsync(model.Email);
                // If the user is found AND Email is confirmed
                if (user != null)//&& await userManager.IsEmailConfirmedAsync(user))
                {
                    // Generate the reset password token
                    var token = await userManager.GeneratePasswordResetTokenAsync(user);

                    // Build the password reset link
                    var passwordResetLink = Url.Action("ResetPassword", "Account",
                            new { email = model.Email, token = token }, Request.Scheme);

                    // Log the password reset link
                    // _logger.Log(LogLevel.Warning, passwordResetLink);
                    try
                    {
                        var mailRequest = new MailRequest()
                        {
                            Subject = "Reset Password Link",
                            ToEmail = model.Email,
                            Body = "<h3>Dear User,</h3><p>Please Use the following link to reset your password for SunBi HR account.</p><p><a href=" + passwordResetLink + ">Reset Password Link</a></p>"
                        };
                        await _mailService.SendEmailAsync(mailRequest);
                        // Send the user to Forgot Password Confirmation view
                    }
                    catch (Exception ex)
                    {
                        //msg = "Error while sending mail" + ExceptionHelper.GetMsg(ex);
                        _logger.LogError(ex, "Error while sending mail.");
                    }

                    return View("ForgotPasswordConfirmation");
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist or is not confirmed
                return View("ForgotPasswordConfirmation");
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string email)
        {
            // If password reset token or email is null, most likely the
            // user tried to tamper the password reset link
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            var model = new ResetPasswordViewModel { Token = token, Email = email };

            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    // reset the user password
                    var result = await userManager.ResetPasswordAsync(user, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        user.IsPasswordUpdated = true;
                        await userManager.UpdateAsync(user);
                        return View("ResetPasswordConfirmation");
                    }
                    // Display validation errors. For example, password reset token already
                    // used to change the password or password complexity rules not met
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist
                return View("ResetPasswordConfirmation");
            }
            // Display validation errors if model state is not valid
            return View(model);
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description.ToString());
            }
        }

        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }

    }
}