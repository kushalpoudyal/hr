﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.Cms;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Helper;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.EmployeeInfo;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using SunbiHR.ViewModel;

namespace SunbiHR.Controllers
{
    [Authorize]
    public class LeaveRequestController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ILogger<LeaveRequestController> _logger;

        public LeaveRequestController(
            SunbiDBContext context
            , UserManager<ApplicationUser> userManager
            , ILogger<LeaveRequestController> logger)
        {
            _context = context;
            this.userManager = userManager;
            _logger = logger;
        }

        // GET: Project
        public async Task<IActionResult> Index(DateTime? fromDate, DateTime? toDate, EmployeeLeaveRequestViewModel employeeLeaveRequestViewModel)
        {
            fromDate = fromDate.HasValue ? fromDate : DateTime.Now;
            toDate = toDate.HasValue ? toDate : DateTime.Now;
            InitCommon(employeeLeaveRequestViewModel);
            ViewData["fromDate"] = fromDate.Value.Date.ToString("yyyy-MM-dd");
            ViewData["toDate"] = toDate.Value.Date.ToString("yyyy-MM-dd");
            var employeeLeaveRequestList = _context.EmployeeLeaveRequest
                .Where(x => x.Deleted_Status == false)
                .Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeInformation)
                .Include(x => x.EmployeeInformation1).AsNoTracking().AsQueryable();
            employeeLeaveRequestList = employeeLeaveRequestList.Where(x => x.CreatedOn.Date >= fromDate.Value.Date && x.CreatedOn.Date <= toDate.Value.Date);

            if (!User.IsInRole("Administrator"))
            {
                employeeLeaveRequestList = employeeLeaveRequestList.Where(x => x.EmployeeId == User.GetEmployeeId());
            }
            if (!string.IsNullOrEmpty(employeeLeaveRequestViewModel?.EmployeeId))
            {
                employeeLeaveRequestList = employeeLeaveRequestList.Where(x => x.EmployeeId == employeeLeaveRequestViewModel.EmployeeId);

            }
            return View(await employeeLeaveRequestList.ToListAsync());
        }

        // GET: Project/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeLeaveRequest = await _context.EmployeeLeaveRequest
                .Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeLeaveRequest == null)
            {
                return NotFound();
            }

            return View(employeeLeaveRequest);
        }

        public IActionResult Create()
        {
            var employeeLeaveRequestViewModel = new EmployeeLeaveRequestViewModel();
            employeeLeaveRequestViewModel.CreatedOn = DateTime.Now;
            InitCommon(employeeLeaveRequestViewModel);
            return View(employeeLeaveRequestViewModel);
        }

        private void InitCommon(EmployeeLeaveRequestViewModel employeeLeaveRequestViewModel)
        {
            ViewBag.LeaveType = new SelectList(Enum.GetValues(typeof(LeaveType)).Cast<LeaveType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString(),
            }).ToList(), "Value", "Text", employeeLeaveRequestViewModel?.LeaveType);
            ViewBag.LeavePeriod = new SelectList(Enum.GetValues(typeof(LeavePeriod)).Cast<LeavePeriod>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString(),
            }).ToList(), "Value", "Text", employeeLeaveRequestViewModel?.LeavePeriod);
            ViewBag.LeavePeriod = new SelectList(EnumHelper.GetEnumValuesAndDescriptions<LeavePeriod>().ToList(), "Value", "Text", employeeLeaveRequestViewModel?.LeavePeriod);
            ViewData["EmployeeId"] = new SelectList(_context.EmployeeInformation, "Id", "Fullname", employeeLeaveRequestViewModel?.EmployeeId);

        }

        // POST: Project/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmployeeLeaveRequestViewModel employeeLeaveRequestViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var employeeLeaveRequest = new EmployeeLeaveRequest();
                    SetValuesFromViewModel(employeeLeaveRequestViewModel, employeeLeaveRequest);

                    employeeLeaveRequest.Id = Guid.NewGuid().ToString();
                    employeeLeaveRequest.CreatedById = User.GetUserId();
                    employeeLeaveRequest.CreatedOn = DateTime.Now;
                    employeeLeaveRequest.LastModifiedDate = DateTime.Now;
                    employeeLeaveRequest.ModifiedTimes = 0;
                    employeeLeaveRequest.Record_Status = Models.Enum.Record_Status.Active;
                    employeeLeaveRequest.Deleted_Status = false;
                    var employeeId = User.GetEmployeeId();
                    if (employeeId==null)
                    {
                        throw new Exception("No employee Id found.");
                    }
                    employeeLeaveRequest.EmployeeId = User.GetEmployeeId();

                    _context.Add(employeeLeaveRequest);
                    await _context.CommitAsync();
                    this.Notify("Leave request created successfully","Notice");
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception ex)
            {
                this.Notify("Error while creating leave request", ExceptionHelper.GetMsg(ex),"error");
                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
            }

            InitCommon(employeeLeaveRequestViewModel);
            return View(employeeLeaveRequestViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveComment(CommentViewModel CommentViewModel)
        {
            try
            {
                var olddata = _context.EmployeeLeaveRequest.FirstOrDefault(x => x.Id == CommentViewModel.Id);
                if (olddata == null)
                {
                    throw new Exception("Invalid Id: " + CommentViewModel.Id);
                }
                if (CommentViewModel.EnumRequestType == EnumRequestType.Approved)
                {
                    olddata.IsApproved = true;
                }
                if (CommentViewModel.EnumRequestType == EnumRequestType.Rejected)
                {
                    olddata.IsRejected = true;
                }
                olddata.ApprovedDate = DateTime.Now;
                olddata.EmployeeId_Approvedby = User.GetEmployeeId();

                _context.Update(olddata);
                await _context.CommitAsync();
                this.Notify("Updated Successfully", "Notice");

            }
            catch (Exception ex)
            {
                this.Notify("Error while updating", ExceptionHelper.GetMsg(ex), "error");
                _logger.LogError(ex, "Error while commenting on leave");
            }
            return RedirectToAction(nameof(Details), new { id = CommentViewModel.Id });
        }
        private static void SetValuesFromViewModel(EmployeeLeaveRequestViewModel employeeLeaveRequestViewModel, EmployeeLeaveRequest employeeLeaveRequest)
        {
            employeeLeaveRequest.Description = employeeLeaveRequestViewModel.Description;
            employeeLeaveRequest.LeaveFrom = employeeLeaveRequestViewModel.LeaveFrom;
            employeeLeaveRequest.LeaveTo = employeeLeaveRequestViewModel.LeaveTo;
            employeeLeaveRequest.LeaveType = employeeLeaveRequestViewModel.LeaveType;
            employeeLeaveRequest.LeavePeriod = employeeLeaveRequestViewModel.LeavePeriod;
            var noOfDays = (employeeLeaveRequest.LeaveFrom.Date - employeeLeaveRequest.LeaveTo.Date).Days;
            employeeLeaveRequest.NoOfDays = noOfDays;

        }

        // GET: Project/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeLeaveRequest = await _context.EmployeeLeaveRequest
                .Include(x => x.EmployeeInformation)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (employeeLeaveRequest == null)
            {
                return NotFound();
            }
            var employeeLeaveRequestViewModel = new EmployeeLeaveRequestViewModel()
            {
                Id = employeeLeaveRequest.Id,
                Description = employeeLeaveRequest.Description,
                //EmployeeId=employeeLeaveRequest.EmployeeId
            };
            InitCommon(employeeLeaveRequestViewModel);
            return View(employeeLeaveRequestViewModel);
        }

        // POST: Project/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, EmployeeLeaveRequestViewModel employeeLeaveRequestViewModel)
        {
            if (id != employeeLeaveRequestViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var OldData = await _context.EmployeeLeaveRequest.FindAsync(id);
                    if (OldData == null)
                    {
                        return NotFound();
                    }
                    OldData.LastModifiedDate = DateTime.Now;
                    OldData.ModifiedTimes += 1;
                    SetValuesFromViewModel(employeeLeaveRequestViewModel, OldData);
                    _context.Update(OldData);
                    await _context.CommitAsync();
                    this.Notify("Leave request edited successfully", "Notice");
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!EmployeeLeaveRequestExists(employeeLeaveRequestViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify("Error while updating", ExceptionHelper.GetMsg(ex), "error");
                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }

                }
                catch (Exception ex)
                {
                    this.Notify("Error while updating", ExceptionHelper.GetMsg(ex), "error");
                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            InitCommon(employeeLeaveRequestViewModel);
            return View(employeeLeaveRequestViewModel);
        }

        // GET: Project/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeLeaveRequest = await _context.EmployeeLeaveRequest
                .Include(e => e.ApplicationUser)
                .Include(x => x.EmployeeInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeLeaveRequest == null)
            {
                return NotFound();
            }

            return View(employeeLeaveRequest);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var employeeLeaveRequest = await _context.EmployeeLeaveRequest.FindAsync(id);
            if (employeeLeaveRequest == null)
            {
                return NotFound();
            }
            employeeLeaveRequest.Deleted_Status = true;
            //_context.EmployeeLeaveRequest.Remove(employeeLeaveRequest);
            _context.Update(employeeLeaveRequest);
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeLeaveRequestExists(string id)
        {
            return _context.EmployeeLeaveRequest.Any(e => e.Id == id);
        }
    }
}
