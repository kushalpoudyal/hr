﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.Cms;
using SunbiHR.DBContext;
using SunbiHR.Helper;
using SunbiHR.Models.Entity;
using SunbiHR.Models.Entity.ProjectInfo;
using SunbiHR.Models.Entity.UserDetails;
using SunbiHR.Models.Enum;
using SunbiHR.ViewModel;
using static SunbiHR.Models.Enum.Enums;
using static SunbiHR.Models.Enum.TaskEnums;

namespace SunbiHR.Controllers
{
    [Authorize(Roles = "Administrator,SuperAdmin")]
    public class ProjectController : Controller
    {
        private readonly SunbiDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ILogger<ProjectController> _logger;

        public ProjectController(
            SunbiDBContext context, 
            UserManager<ApplicationUser> userManager,
            ILogger<ProjectController> logger)
        {
            _context = context;
            this.userManager = userManager;
            _logger = logger;
        }

        // GET: Project
        public async Task<IActionResult> Index()
        {
            var sunbiDBContext = _context.ProjectInformation.Where(x => x.Deleted_Status == false).Include(e => e.ApplicationUser).Include
                (x => x.EmployeeProjects).ThenInclude(x => x.EmployeeInformation);
            return View(await sunbiDBContext.ToListAsync());
        }

        // GET: Project/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projectInformation = await _context.ProjectInformation
                .Include(e => e.ApplicationUser)
                .Include(x=>x.Milestone)
                .ThenInclude(x=>x.TaskInformation)
                .Include(x=>x.TaskInformation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (projectInformation == null)
            {
                return NotFound();
            }
            ViewData["Status"] = new SelectList(EnumHelper.GetItems<EnumMilestoneStatus>(), "Value", "Text");
            ViewData["EnumTaskStatus"] = new SelectList(EnumHelper.GetItems<EnumTaskStatus>(), "Value", "Text");
            ViewData["Milestone"] = new SelectList(_context.Milestone.Where(x=>x.ProjectId==id).ToList(), "Id", "Title");
            ViewData["EmployeeIds"] = new MultiSelectList(_context.EmployeeInformation, "Id", "Fullname");


            return View(projectInformation);
        }

        public async Task<JsonResult> GetProjectDetailsById(string id)
        {
            try
            {

                if (id == null)
                {
                    throw new Exception("Id is null");
                }

                var projectInformation = await _context.ProjectInformation
                    .Include(e => e.ApplicationUser)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (projectInformation == null)
                {
                    throw new Exception("Data not found with id: " + id);
                }
                return Json(new { Status = true, ErrorMsg = "", Result = projectInformation });

            }
            catch (Exception ex)
            {

                return Json(new { Status = false, ErrorMsg = ExceptionHelper.GetMsg(ex), DataList = "" });
            }
        }

        public IActionResult Create()
        {
            var projectViewModel = new ProjectViewModel();
            InitCommon(projectViewModel);
            return View(projectViewModel);
        }

        private void InitCommon(ProjectViewModel model)
        {
            ViewData["Employees"] = new MultiSelectList(_context.EmployeeInformation, "Id", "Fullname", model?.EmployeeProjects.Select(x => x.EmployeeId).ToArray());
            ViewData["Status"] = new SelectList(EnumHelper.GetItems<EnumProjectStatus>(), "Value", "Text",(byte) model?.Status);
            
        }

        // POST: Project/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProjectViewModel projectViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var projectInformation = new ProjectInformation();

                    projectInformation.Id = Guid.NewGuid().ToString();
                    projectInformation.CreatedById = this.userManager.GetUserId(HttpContext.User);
                    projectInformation.CreatedOn = DateTime.Now;
                    projectInformation.LastModifiedDate = DateTime.Now;
                    projectInformation.ModifiedTimes = 0;
                    projectInformation.Record_Status = Models.Enum.Record_Status.Active;
                    projectInformation.Deleted_Status = false;
                    projectInformation.Title = projectViewModel.Title;
                    projectInformation.Description = projectViewModel.Description;
                    projectInformation.StartedDate = projectViewModel.StartedDate;
                    projectInformation.EstimatedEndDate = projectViewModel.EstimatedEndDate;
                    projectInformation.Status = projectViewModel.Status;

                    if (projectViewModel.Employees != null)
                    {
                        foreach (var employeeId in projectViewModel.Employees)
                        {
                            projectInformation.EmployeeProjects.Add(new EmployeeProject()
                            {
                                EmployeeId = employeeId
                            });
                        }
                    }
                    if (projectViewModel.Milestone != null)
                    {
                        foreach (var milestone in projectViewModel.Milestone)
                        {
                            milestone.Id = Guid.NewGuid().ToString();
                            milestone.CreatedById = this.userManager.GetUserId(HttpContext.User);
                            milestone.CreatedOn = DateTime.Now;
                            milestone.LastModifiedDate = DateTime.Now;
                            milestone.ModifiedTimes = 0;
                            milestone.Record_Status = Models.Enum.Record_Status.Active;
                            milestone.Deleted_Status = false;
                            projectInformation.Milestone.Add(milestone);
                        }
                    }
                    _context.Add(projectInformation);
                    await _context.CommitAsync();
                    this.Notify("Project Created Successfully", "Notice");

                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                this.Notify("Error while creating project" + ExceptionHelper.GetMsg(ex), "Error", "error");

                _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
            }

            InitCommon(projectViewModel);
            return View(projectViewModel);
        }

        // GET: Project/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projectInformation = await _context.ProjectInformation.Include(x => x.EmployeeProjects)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (projectInformation == null)
            {
                return NotFound();
            }
            var projectViewModel = new ProjectViewModel()
            {
                Id = projectInformation.Id,
                Description = projectInformation.Description,
                EmployeeProjects = projectInformation.EmployeeProjects,
                Title = projectInformation.Title,
                StartedDate = projectInformation.StartedDate,
                EstimatedEndDate=projectInformation.EstimatedEndDate,
                Status=projectInformation.Status,
            };
            InitCommon(projectViewModel);
            return View(projectViewModel);
        }

        // POST: Project/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, ProjectViewModel projectViewModel)
        {
            if (id != projectViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var OldData = await _context.ProjectInformation.Include(x=>x.EmployeeProjects).FirstOrDefaultAsync(x=>x.Id==id);
                    if (OldData == null)
                    {
                        return NotFound();
                    }
                    OldData.LastModifiedDate = DateTime.Now;
                    OldData.ModifiedTimes += 1;
                    OldData.Title = projectViewModel.Title;
                    OldData.Description = projectViewModel.Description;
                    OldData.StartedDate = projectViewModel.StartedDate;
                    OldData.EstimatedEndDate = projectViewModel.EstimatedEndDate;
                    OldData.Status = projectViewModel.Status;
                    _context.RemoveRange(OldData.EmployeeProjects);
                    if (projectViewModel.Employees != null)
                    {
                        foreach (var employeeId in projectViewModel.Employees)
                        {
                            OldData.EmployeeProjects.Add(new EmployeeProject()
                            {
                                EmployeeId = employeeId
                            });
                        }
                    }
                    //_context.RemoveRange(OldData.Milestone);

                    if (projectViewModel.Milestone != null)
                    {

                        foreach (var milestone in projectViewModel.Milestone)
                        {
                            milestone.Id = Guid.NewGuid().ToString();
                            milestone.LastModifiedDate = DateTime.Now;
                            milestone.ModifiedTimes += 1;
                            OldData.Milestone.Add(milestone);
                        }
                    }
                    _context.Update(OldData);
                    await _context.CommitAsync();
                    this.Notify("Project Updated Successfully", "Notice");

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!ProjectInformationExists(projectViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        this.Notify("Error while creating project" + ExceptionHelper.GetMsg(ex), "Error", "error");

                        _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                    }
                }
                catch (Exception ex)
                {
                    this.Notify("Error while creating project" + ExceptionHelper.GetMsg(ex), "Error", "error");

                    _logger.LogError(ex, ExceptionHelper.GetMsg(ex));
                }
                return RedirectToAction(nameof(Index));
            }
            InitCommon(projectViewModel);
            return View(projectViewModel);
        }

        // GET: Project/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projectInformation = await _context.ProjectInformation
                .Include(e => e.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (projectInformation == null)
            {
                return NotFound();
            }

            return View(projectInformation);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var projectInformation = await _context.ProjectInformation.FindAsync(id);
            if (projectInformation == null)
            {
                return NotFound();
            }
            projectInformation.Deleted_Status = true;
            //_context.ProjectInformation.Remove(projectInformation);
            _context.Update(projectInformation);
            await _context.CommitAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProjectInformationExists(string id)
        {
            return _context.ProjectInformation.Any(e => e.Id == id);
        }
    }
}
