﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunbiHR.DBContext;
using SunbiHR.Extensions;
using SunbiHR.Models;
using SunbiHR.Models.Entity.TaskInfo;
using SunbiHR.Models.Enum;

namespace SunbiHR.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly SunbiDBContext _context;

        public HomeController(ILogger<HomeController> logger,SunbiDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var PendingTask = new List<TaskInformation>();
            var currentEmployeeId = User.GetEmployeeId();
            if (currentEmployeeId!=null)
            {
                PendingTask = await _context.TaskInformation
                    .Include(x=>x.TaskEmployee).Include(p=>p.ProjectInformation)
                    .Include(m=>m.Milestone)
                    .Where(x => x.TaskStatus == TaskEnums.EnumTaskStatus.Open && x.TaskEmployee.Any(x=>x.EmployeeId==currentEmployeeId)).ToListAsync();
            }
            
            ViewBag.PendingTask = PendingTask;
            ViewBag.EmployeeCount = await _context.EmployeeInformation.CountAsync() ;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
