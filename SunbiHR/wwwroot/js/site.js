﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function ReApply() {
    $(".search-select").each(function () {
        $(this).select2(
            {
                placeholder: 'Select an option',
                allowClear: true
            }
        );
    });

    var myDate = new Date('0001-01-01T00:00:00Z');
    $('.datepicker').each(function () {
        if ($(this).val() == myDate || $(this).val() == '0001-01-01') {
            console.log("not null");
            $(this).val(new Date().toISOString().slice(0, 10));
        }
    });
    $(".datepicker").each(function () {
        $(this).datepicker({
            flat: true,
            dateFormat: "yy-mm-dd"
        });

    });
    $(".dataTable").each(function () {
        $(this).addClass("w-100");
    });

    $(".dataTable").each(function () {
        $(this).DataTable({
            "scrollX": true
        });

    });
}
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
});

$(document).ready(function () {
    ReApply();
    $(document).ajaxStart(function () {
        $(":submit").prop('disabled', true);
    }).ajaxStop(function () {
        $(":submit").prop('disabled', false);
    });
});
$(document).on('submit', 'form.ajaxSubmit', function (e) {
    console.log('checking....', $(this).valid());
    if (!$(this).valid()) {
        //validation failed
        return false;
    }
    $(this).find(":submit").attr("disabled", true);

    var callBack = window[$(this).data('callback')];
    console.log("callback=" + typeof callBack);
    var form = $(this)[0];
    $postURL = $(this).attr('action');
    $targetdiv = $(this).data('refresh');
    $targeturl = $($targetdiv).data("url");
    var data = new FormData(form);
    //data = $(this).serialize();
    //showLoading();
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: $postURL,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            //hideLoading();
            console.log(response);
            var stat = response.status;
            console.log("stat=" + stat);
            if (stat) {
                  $('#editMilestone').modal('hide');
                $('#editTask').modal('hide');
                //$(".ajaxSubmit")[0].reset();
                console.log("check");
                console.log("callback=" + typeof callBack);
                toastr["success"](response.msg);
                $($targetdiv).each(function () {
                    $(this).load($targeturl);
                })
                if (typeof callBack == "function") {
                    callBack(response);
                }
                //$(this).closest('.modal').modal('destory');
              
            } else {
                toastr["error"](response.msg);
            }
        }, error: function (err) {
            hideLoading();
            toastr["error"](error);
            console.log("error", err);
        }
    });
    $(this).find(":submit").removeAttr("disabled");

    console.log('showing modal');
    e.preventDefault();
    return false;

});