﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR
{
    public static class ViewHelper
    {

        public static void Notify(this Controller controller, string message, string title = "Toastr",
                                    string notificationType = "success")
        {
            var msg = new
            {
                message = message,
                title = title,
                icon = notificationType.ToString(),
                type = notificationType.ToString(),
            };

           controller.TempData["Message"] = JsonConvert.SerializeObject(msg);
        }
    }
}
