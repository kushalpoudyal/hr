﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace SunbiHR.Helper
{
    public static class UniqueCode
    {
        public static string GetCode(string Module)
        {
            return string.Format("{0}-{1}-{2}{3}-{4}{5}{6}", Module, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }
    }

}
