﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SunbiHR.Helper
{
    public static class EnumHelper
    {
        public static List<SelectListItem> GetItems<T>() where T : struct, IConvertible
        {
            var list = Enum.GetValues(typeof(T)).Cast<T>().Select(v => new SelectListItem
            {
                Text =v.ToString(),
                Value = (v.ToInt32(CultureInfo.InvariantCulture.NumberFormat)).ToString(),
            }).ToList();
            return list;
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static List<SelectListItem> GetEnumValuesAndDescriptions<T>()
        {
            Type enumType = typeof(T);

            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T is not System.Enum");

            List<SelectListItem> enumValList = new List<SelectListItem>();

            foreach (Enum e in Enum.GetValues(typeof(T)))
            {
                var fi = e.GetType().GetField(e.ToString());
                var attributes = (DisplayAttribute[])fi.GetCustomAttributes(typeof(DisplayAttribute), false);

                enumValList.Add(new SelectListItem()
                {
                    Text = (attributes.Length > 0) ? attributes[0].Name : e.ToString(),
                    Value = Convert.ToInt32(e).ToString()
                });

            }
        

            return enumValList;
        }
        public static string GetDescription(this Enum enumValue)
        {
            object[] attr = enumValue.GetType().GetField(enumValue.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attr.Length > 0
               ? ((DescriptionAttribute)attr[0]).Description
               : enumValue.ToString();
        }

    }
}
