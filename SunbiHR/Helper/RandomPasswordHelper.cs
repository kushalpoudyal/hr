﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SunbiHR.Helper
{
    public static class RandomPasswordHelper
    {
        const string LOWER_CASE = "abcdefghijklmnopqursuvwxyz";
        const string UPPER_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const string NUMBERS = "123456789";
        const string SPECIALS = @"!@£$%^&*()#€";
        const string PASSWORDPATTERN = @"^(.{0,7}|[^0-9]*|[^A-Z]*|[^a-z]*|[a-zA-Z0-9]*)$";

        public static string GeneratePassword(
            bool useLowercase = true,
            bool useUppercase = true,
            bool useNumbers = true,
            bool useSpecial = true,
            int passwordSize = 20)
        {
            char[] _password = new char[passwordSize];
            string charSet = ""; // Initialise to blank
            System.Random _random = new Random();
            int counter;

            // Build up the character set to choose from
            if (useLowercase) charSet += LOWER_CASE;

            if (useUppercase) charSet += UPPER_CASE;

            if (useNumbers) charSet += NUMBERS;

            if (useSpecial) charSet += SPECIALS;

            for (counter = 0; counter < passwordSize; counter++)
            {
                _password[counter] = charSet[_random.Next(charSet.Length - 1)];
            }
            var password = String.Join(null, _password);
            bool result = Regex.IsMatch(password, PASSWORDPATTERN);//check whether it has more than 8 characters OR atleast 1 numbers OR atleast 1 uppercase OR or atleast 1 lowercase OR atleast 1 special characters.
            if (!result)
            {
                GeneratePassword();//if string not match with regex regenerate password
            }
            return password;
        }
    }
}
