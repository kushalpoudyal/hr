﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SunbiHR.Helper
{
    public static class ExceptionHelper
    {
        public static string GetMsg(Exception ex)
        {
            StringBuilder builder = new StringBuilder("");
            if (ex.Message==null)
            {
                if (ex.InnerException!=null)
                {
                    builder =builder.Append("Message"+ ex.InnerException.Message);
                    return builder.ToString();
                }
            }
            builder = builder.Append("Message " + ex.Message);
            builder = builder.Append("/nStrackTrace " + ex.StackTrace);
            return builder.ToString();
        }
    }

}
