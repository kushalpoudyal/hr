﻿using SunbiHR.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.DAL
{
    public class UoW : IUoW
    {
        private ITaskRepository _task;
        private SunbiDBContext _sunbiDBContext;

        public UoW(SunbiDBContext sunbiDBContext)
        {
            _sunbiDBContext = sunbiDBContext;
        }

        public ITaskRepository TaskRepository
        {
            get
            {
                if (_task == null)
                {
                    _task = new TaskRepository(_sunbiDBContext);
                }
                return _task;
            }
        }

        public async Task SaveAsync()
        {
            await _sunbiDBContext.SaveChangesAsync();
        }
    }
}
