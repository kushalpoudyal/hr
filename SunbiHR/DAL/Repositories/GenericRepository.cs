﻿using Microsoft.EntityFrameworkCore;
using SunbiHR.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SunbiHR.DAL
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected SunbiDBContext SunbiDBContext { get; set; }
        private DbSet<T> entities;
        public GenericRepository(SunbiDBContext sunbiDBContext)
        {
            this.SunbiDBContext = sunbiDBContext;
            entities = sunbiDBContext.Set<T>();
        }
        public IQueryable<T> FindAll()
        {
            return entities.AsNoTracking();
        }
        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return entities
                .Where(expression).AsNoTracking();
        }
        public void Create(T entity)
        {
            entities.Add(entity);
        }
        public void Update(T entity)
        {
            entities.Update(entity);
        }
        public void Delete(T entity)
        {
            entities.Remove(entity);
        }
    }
}

