﻿using SunbiHR.DBContext;
using SunbiHR.Models.Entity.TaskInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SunbiHR.DAL
{
    public class TaskRepository : GenericRepository<TaskInformation>, ITaskRepository
    {
        public TaskRepository(SunbiDBContext sunbiDBContext) : base(sunbiDBContext)
        {
        }
    }
}
