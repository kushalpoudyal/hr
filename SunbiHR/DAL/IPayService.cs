﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.DAL
{
    public interface IPayService
    {
        public Task<decimal> TotalDueAmount(string EmpId);

        public Task<DateTime> DueSince(string EmpId);
    }
}
