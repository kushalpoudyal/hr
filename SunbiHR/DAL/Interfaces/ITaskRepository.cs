﻿using SunbiHR.Models.Entity.TaskInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SunbiHR.DAL
{
    public interface ITaskRepository:IGenericRepository<TaskInformation>
    {

    }
}
