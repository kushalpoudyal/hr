﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.DAL
{
    public interface IUoW
    {
        ITaskRepository TaskRepository { get; }
        Task SaveAsync();
    }
}
