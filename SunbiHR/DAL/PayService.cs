﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Schema;
using SunbiHR.DBContext;
using SunbiHR.Models.Entity.EmployeeInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SunbiHR.DAL
{
    public class PayService : Repository<EmployeePayment>, IPayService
    {
        public PayService(SunbiDBContext context) : base(context)
        {
        }

        public async Task<DateTime> DueSince(string EmpId)
        {
            var LastPay = await context.EmployeePayment.Where(x => x.EmployeeId == EmpId).OrderByDescending(x => x.TransactionOn).FirstOrDefaultAsync();
            var EmpTim = LastPay == null ? context.EmployeeTimline.Where(x => x.EmployeeId == EmpId).ToList()
                : context.EmployeeTimline.Where(x => x.EmployeeId == EmpId && x.CommencedFrom >= GetSalDate((int)LastPay.PaymentMonth, LastPay.PaymentYear) && x.CommencedFrom <= DateTime.Now).ToList();
            if (EmpTim.Count == 0)
            {
                return DateTime.Now;
            }
            var GetLastPay = LastPay == null ? DateTime.Now : GetSalDate((int)LastPay.PaymentMonth, LastPay.PaymentYear);
            var LastPaidMonth = LastPay == null ? EmpTim.OrderBy(x => x.CommencedFrom).Select(x => x.CommencedFrom.Month).FirstOrDefault() : GetLastPay.AddMonths(1).Month;
            var LastPaidYear = LastPay == null ? EmpTim.OrderBy(x => x.CommencedFrom).Select(x => x.CommencedFrom.Year).FirstOrDefault() : GetLastPay.AddMonths(1).Year;

            return GetSalDate(LastPaidMonth, LastPaidYear);

        }

        public async Task<decimal> TotalDueAmount(string EmpId)
        {


            var LastPay = await context.EmployeePayment.Where(x => x.EmployeeId == EmpId).OrderByDescending(x => x.TransactionOn).FirstOrDefaultAsync();
            var EmpTim = LastPay == null ? context.EmployeeTimline.Where(x => x.EmployeeId == EmpId).ToList()
                : context.EmployeeTimline.Where(x => x.EmployeeId == EmpId && x.CommencedFrom >= GetSalDate((int)LastPay.PaymentMonth, LastPay.PaymentYear) && x.CommencedFrom <= DateTime.Now).ToList();
            if (EmpTim.Count == 0)
            {
                return 0;
            }
            var CurrentScheme = EmpTim.Where(x => x.Current == true).FirstOrDefault();
            var CurrentSalary = CurrentScheme.Amount;
            var SalFrom = CurrentScheme.CommencedFrom;
            var GetLastPay = LastPay == null ? DateTime.Now : GetSalDate((int)LastPay.PaymentMonth, LastPay.PaymentYear);
            var LastPaidMonth = LastPay == null ? EmpTim.OrderBy(x => x.CommencedFrom).Select(x => x.CommencedFrom.Month).FirstOrDefault() : GetLastPay.AddMonths(1).Month;
            var LastPaidYear = LastPay == null ? EmpTim.OrderBy(x => x.CommencedFrom).Select(x => x.CommencedFrom.Year).FirstOrDefault() : GetLastPay.AddMonths(1).Year;
            if (EmpTim.Count() > 1)
            {
                decimal TotalSal = new decimal();
                var AllSalary = EmpTim.Distinct().Select(x => new { x.CommencedFrom, x.Amount }).ToList();

                for (int i = 0; i < AllSalary.Count(); i++)
                {
                    if (i == 0)
                    {
                        TotalSal += UnPaidMonthsBySalary(LastPaidMonth, LastPaidYear, AllSalary[i].CommencedFrom.Month, AllSalary[i].CommencedFrom.Year, AllSalary[i].Amount);
                    }
                    else
                    {
                        int j = i - 1;
                        TotalSal += UnPaidMonthsBySalary(AllSalary[j].CommencedFrom.Month, AllSalary[j].CommencedFrom.Year, AllSalary[i].CommencedFrom.Month, AllSalary[i].CommencedFrom.Year, AllSalary[i].Amount);
                    }

                }



                return TotalSal;
            }
            else
            {
                var CurrMonth = DateTime.Now.Month;
                var CurrYear = DateTime.Now.Year;
                return UnPaidMonthsBySalary(LastPaidMonth, LastPaidYear, CurrMonth, CurrYear, CurrentSalary);
            }







        }
        private decimal UnPaidMonthsBySalary(int LastPaidM, int LastPaidY, int CurrMonth, int CurrYear, decimal Sal)
        {
            DateTime Lastdt = GetSalDate(LastPaidM, LastPaidY);
            DateTime CurrDt = GetSalDate(CurrMonth, CurrYear);
            var totalDays = Convert.ToInt32(((CurrDt - Lastdt).TotalDays) / 30);
            var totalSal = totalDays * Sal;
            return totalSal;


        }

        private DateTime GetSalDate(int LastPaidM, int LastPaidY)
        {
            DateTime date = LastPaidM.ToString().Length == 1 ? Convert.ToDateTime("0" + LastPaidM.ToString() + "/01/" + LastPaidY.ToString()) : Convert.ToDateTime(LastPaidM.ToString() + "/01/" + LastPaidY.ToString());
            return date;
        }
    }
}
