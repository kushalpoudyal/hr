﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class add_user_employee_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeeUser",
                columns: table => new
                {
                    UserId = table.Column<string>(maxLength: 36, nullable: false),
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedById = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false),
                    Deleted_Status = table.Column<bool>(nullable: false),
                    EmployeeId = table.Column<string>(maxLength: 36, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeUser", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_EmployeeUser_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeUser_EmployeeInformation_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "EmployeeInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeUser_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeUser_CreatedById",
                table: "EmployeeUser",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeUser_EmployeeId",
                table: "EmployeeUser",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeUser_UserId",
                table: "EmployeeUser",
                column: "UserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeUser");
        }
    }
}
