﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class add_multiple_employee_table_for_task : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskInformation_EmployeeInformation_EmployeeId",
                table: "TaskInformation");

            migrationBuilder.DropIndex(
                name: "IX_TaskInformation_EmployeeId",
                table: "TaskInformation");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "TaskInformation");

            migrationBuilder.AddColumn<string>(
                name: "EmployeeInformationId",
                table: "TaskInformation",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Current",
                table: "EmployeeTimline",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "IsRejected",
                table: "EmployeeLeaveRequest",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "IsApproved",
                table: "EmployeeLeaveRequest",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "TwoFactorEnabled",
                table: "ApplicationUser",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "PhoneNumberConfirmed",
                table: "ApplicationUser",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "LockoutEnabled",
                table: "ApplicationUser",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "IsPasswordUpdated",
                table: "ApplicationUser",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "ApplicationUser",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "ApplicationUser",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "EmailConfirmed",
                table: "ApplicationUser",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "bit");

            migrationBuilder.CreateTable(
                name: "TaskEmployee",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedById = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false),
                    Deleted_Status = table.Column<ulong>(type: "bit", nullable: false),
                    TaskId = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskEmployee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskEmployee_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskEmployee_EmployeeInformation_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "EmployeeInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskEmployee_TaskInformation_TaskId",
                        column: x => x.TaskId,
                        principalTable: "TaskInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaskInformation_EmployeeInformationId",
                table: "TaskInformation",
                column: "EmployeeInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskEmployee_CreatedById",
                table: "TaskEmployee",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_TaskEmployee_EmployeeId",
                table: "TaskEmployee",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskEmployee_TaskId",
                table: "TaskEmployee",
                column: "TaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInformation_EmployeeInformation_EmployeeInformationId",
                table: "TaskInformation",
                column: "EmployeeInformationId",
                principalTable: "EmployeeInformation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskInformation_EmployeeInformation_EmployeeInformationId",
                table: "TaskInformation");

            migrationBuilder.DropTable(
                name: "TaskEmployee");

            migrationBuilder.DropIndex(
                name: "IX_TaskInformation_EmployeeInformationId",
                table: "TaskInformation");

            migrationBuilder.DropColumn(
                name: "EmployeeInformationId",
                table: "TaskInformation");

            migrationBuilder.AddColumn<string>(
                name: "EmployeeId",
                table: "TaskInformation",
                type: "varchar(36)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<ulong>(
                name: "Current",
                table: "EmployeeTimline",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "IsRejected",
                table: "EmployeeLeaveRequest",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "IsApproved",
                table: "EmployeeLeaveRequest",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "TwoFactorEnabled",
                table: "ApplicationUser",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "PhoneNumberConfirmed",
                table: "ApplicationUser",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "LockoutEnabled",
                table: "ApplicationUser",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "IsPasswordUpdated",
                table: "ApplicationUser",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "IsDeleted",
                table: "ApplicationUser",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "IsActive",
                table: "ApplicationUser",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<ulong>(
                name: "EmailConfirmed",
                table: "ApplicationUser",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.CreateIndex(
                name: "IX_TaskInformation_EmployeeId",
                table: "TaskInformation",
                column: "EmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInformation_EmployeeInformation_EmployeeId",
                table: "TaskInformation",
                column: "EmployeeId",
                principalTable: "EmployeeInformation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
