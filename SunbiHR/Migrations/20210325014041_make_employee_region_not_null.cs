﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class make_employee_region_not_null : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeInformation_Region_RegionId",
                table: "EmployeeInformation");

            migrationBuilder.AlterColumn<string>(
                name: "RegionId",
                table: "EmployeeInformation",
                maxLength: 36,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)",
                oldMaxLength: 36,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeInformation_Region_RegionId",
                table: "EmployeeInformation",
                column: "RegionId",
                principalTable: "Region",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeInformation_Region_RegionId",
                table: "EmployeeInformation");

            migrationBuilder.AlterColumn<string>(
                name: "RegionId",
                table: "EmployeeInformation",
                type: "varchar(36)",
                maxLength: 36,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 36);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeInformation_Region_RegionId",
                table: "EmployeeInformation",
                column: "RegionId",
                principalTable: "Region",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
