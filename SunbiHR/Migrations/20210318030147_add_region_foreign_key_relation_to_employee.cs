﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class add_region_foreign_key_relation_to_employee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RegionId",
                table: "EmployeeInformation",
                maxLength: 36,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeInformation_RegionId",
                table: "EmployeeInformation",
                column: "RegionId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeInformation_Region_RegionId",
                table: "EmployeeInformation",
                column: "RegionId",
                principalTable: "Region",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeInformation_Region_RegionId",
                table: "EmployeeInformation");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeInformation_RegionId",
                table: "EmployeeInformation");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "EmployeeInformation");
        }
    }
}
