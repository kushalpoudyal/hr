﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class add_employee_leave_request_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DueDate",
                table: "TaskInformation",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TaskStatus",
                table: "TaskInformation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "EmployeeLeaveRequest",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedById = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false),
                    Deleted_Status = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(maxLength: 36, nullable: false),
                    LeaveType = table.Column<int>(nullable: false),
                    LeaveFrom = table.Column<DateTime>(nullable: false),
                    LeaveTo = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 2000, nullable: false),
                    IsApproved = table.Column<bool>(nullable: false),
                    IsRejected = table.Column<bool>(nullable: false),
                    EmployeeId = table.Column<string>(maxLength: 36, nullable: false),
                    EmployeeId_Approvedby = table.Column<string>(maxLength: 36, nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    ApprovedRemarks = table.Column<string>(maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeLeaveRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeeLeaveRequest_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeLeaveRequest_EmployeeInformation_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "EmployeeInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeLeaveRequest_EmployeeInformation_EmployeeId_Approved~",
                        column: x => x.EmployeeId_Approvedby,
                        principalTable: "EmployeeInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeLeaveRequest_CreatedById",
                table: "EmployeeLeaveRequest",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeLeaveRequest_EmployeeId",
                table: "EmployeeLeaveRequest",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeLeaveRequest_EmployeeId_Approvedby",
                table: "EmployeeLeaveRequest",
                column: "EmployeeId_Approvedby");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeLeaveRequest");

            migrationBuilder.DropColumn(
                name: "DueDate",
                table: "TaskInformation");

            migrationBuilder.DropColumn(
                name: "TaskStatus",
                table: "TaskInformation");
        }
    }
}
