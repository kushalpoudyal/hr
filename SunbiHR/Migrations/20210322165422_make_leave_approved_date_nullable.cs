﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace SunbiHR.Migrations
{
    public partial class make_leave_approved_date_nullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
               name: "ApprovedDate",
               table: "EmployeeLeaveRequest",
               nullable: true,
               oldClrType: typeof(DateTime),
               oldType: "datetime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
