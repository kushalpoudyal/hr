﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class added_projects_for_daily_report : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DailyReportProject",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedById = table.Column<string>(maxLength: 36, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false),
                    Deleted_Status = table.Column<ulong>(type: "bit", nullable: false),
                    DailyReportId = table.Column<string>(maxLength: 36, nullable: false),
                    ProjectId = table.Column<string>(maxLength: 36, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DailyReportProject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DailyReportProject_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DailyReportProject_EmployeeDailyReport_DailyReportId",
                        column: x => x.DailyReportId,
                        principalTable: "EmployeeDailyReport",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DailyReportProject_ProjectInformation_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DailyReportProject_CreatedById",
                table: "DailyReportProject",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_DailyReportProject_DailyReportId",
                table: "DailyReportProject",
                column: "DailyReportId");

            migrationBuilder.CreateIndex(
                name: "IX_DailyReportProject_ProjectId",
                table: "DailyReportProject",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DailyReportProject");
        }
    }
}
