﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class Identity3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeePayment",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    RefNo = table.Column<string>(maxLength: 100, nullable: false),
                    EmployeeId = table.Column<string>(maxLength: 36, nullable: true),
                    PaymentMonth = table.Column<string>(maxLength: 36, nullable: true),
                    PaymentRemarks = table.Column<string>(maxLength: 1000, nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    TransactionOn = table.Column<DateTime>(nullable: false),
                    PaidBy = table.Column<string>(maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeePayment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeePayment_EmployeeInformation_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "EmployeeInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeePayment_ApplicationUser_PaidBy",
                        column: x => x.PaidBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeePayment_EmployeeId",
                table: "EmployeePayment",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeePayment_PaidBy",
                table: "EmployeePayment",
                column: "PaidBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeePayment");
        }
    }
}
