﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class fix_migration_error : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskInformation_EmployeeInformation_EmployeeInformationId",
                table: "TaskInformation");

            migrationBuilder.DropIndex(
                name: "IX_TaskInformation_EmployeeInformationId",
                table: "TaskInformation");

            migrationBuilder.DropColumn(
                name: "EmployeeInformationId",
                table: "TaskInformation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmployeeInformationId",
                table: "TaskInformation",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaskInformation_EmployeeInformationId",
                table: "TaskInformation",
                column: "EmployeeInformationId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInformation_EmployeeInformation_EmployeeInformationId",
                table: "TaskInformation",
                column: "EmployeeInformationId",
                principalTable: "EmployeeInformation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
