﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class remove_title_form_leave : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title",
                table: "EmployeeLeaveRequest");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "EmployeeLeaveRequest",
                type: "varchar(36)",
                maxLength: 36,
                nullable: false,
                defaultValue: "");
        }
    }
}
