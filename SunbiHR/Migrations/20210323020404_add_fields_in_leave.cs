﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class add_fields_in_leave : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LeavePeriod",
                table: "EmployeeLeaveRequest",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NoOfDays",
                table: "EmployeeLeaveRequest",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LeavePeriod",
                table: "EmployeeLeaveRequest");

            migrationBuilder.DropColumn(
                name: "NoOfDays",
                table: "EmployeeLeaveRequest");
        }
    }
}
