﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunbiHR.Migrations
{
    public partial class payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PaymentMonth",
                table: "EmployeePayment",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)",
                oldMaxLength: 36,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentYear",
                table: "EmployeePayment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "EmployeeTimline",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    EmployeeId = table.Column<string>(maxLength: 36, nullable: true),
                    NewPosition = table.Column<string>(maxLength: 200, nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 200, nullable: true),
                    CommencedFrom = table.Column<DateTime>(nullable: false),
                    Current = table.Column<bool>(nullable: false),
                    CreatedById = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeTimline", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeeTimline_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeTimline_EmployeeInformation_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "EmployeeInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeTimline_CreatedById",
                table: "EmployeeTimline",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeTimline_EmployeeId",
                table: "EmployeeTimline",
                column: "EmployeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeTimline");

            migrationBuilder.DropColumn(
                name: "PaymentYear",
                table: "EmployeePayment");

            migrationBuilder.AlterColumn<string>(
                name: "PaymentMonth",
                table: "EmployeePayment",
                type: "varchar(36)",
                maxLength: 36,
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
