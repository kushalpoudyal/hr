﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using SunbiHR.DBContext;
using SunbiHR.Models.Entity.UserDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SunbiHR.Customs
{
    public class AdditionalUserClaimsPrincipalFactory
        : UserClaimsPrincipalFactory<ApplicationUser, ApplicationRole>
    {
        private readonly SunbiDBContext _context;

        public AdditionalUserClaimsPrincipalFactory(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<IdentityOptions> optionsAccessor,
            SunbiDBContext context)
            : base(userManager, roleManager, optionsAccessor)
        {
            _context = context;
        }

        public async override Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        {
            var principal = await base.CreateAsync(user);
            var identity = (ClaimsIdentity)principal.Identity;
            if (user!=null)
            {
                var usrRole = await this.UserManager.GetRolesAsync(user);
                var claims = new List<Claim>();
                if (usrRole.Any())
                {
                    foreach (var item in usrRole)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, item));
                    }
                }
                //claims.Add(new Claim(ClaimTypes.NameIdentifier, usrdetails.Id.ToString()));
                var EmployeeUser = _context.EmployeeUser.FirstOrDefault(x => x.UserId == user.Id);
                if (EmployeeUser == null)
                {
                    //no user employee relation found
                    //check for super admin otherwise throw error
                }
                else
                {
                    claims.Add(new Claim("EmployeeId", EmployeeUser.EmployeeId.ToString()));

                }
                var appIdentity = new ClaimsIdentity(claims);
                identity.AddClaims(claims);
            }

            return principal;
        }
    }
}
