using System;
using Xunit;

namespace SunbiHr.UnitTests
{
    public class MathOperationTest
    {
        [Fact]
        public void Task_Add_TwoNumber()
        {
            // Arrange  
            var num1 = 2.9;
            var num2 = 3.1;
            var expectedValue = 6;

            // Act  
            var sum =6;

            //Assert  
            Assert.Equal(expectedValue, sum);
        }
    }
}
